package org.undp.ekyzmatker.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.undp.ekyzmatker.database.DbContract;
import org.undp.ekyzmatker.entity.ServiceGroupDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksey on 28.05.15.
 */
public class SQLiteServiceGroupDao implements ServiceGroupDao {

    private SQLiteDatabase mDb;

    public SQLiteServiceGroupDao(SQLiteDatabase db) {
        mDb = db;
    }

    @Override
    public long insert(ServiceGroupDto group) {
        ContentValues v = new ContentValues();

        v.put(DbContract.TableServiceGroup.COLUMN_NAME_NAME, group.getName());
        v.put(DbContract.TableServiceGroup.COLUMN_NAME_DESCRIPTION, group.getDesc());

        return mDb.insert(DbContract.TableServiceGroup.TABLE_NAME, null, v);
    }

    @Override
    public boolean update(ServiceGroupDto group) {
        return false;
    }

    @Override
    public boolean delete(ServiceGroupDto group) {
        return false;
    }

    @Override
    public ServiceGroupDto findById(long id) {

        return null;
    }

    @Override
    public List<ServiceGroupDto> findAll() {
        ArrayList<ServiceGroupDto> groups = null;

        Cursor c = mDb.query(DbContract.TableServiceGroup.TABLE_NAME, null, null, null, null, null, null);

        if (c.getCount() != 0) {
            groups = new ArrayList<ServiceGroupDto>();
            while(c.moveToNext()) {
                groups.add(new ServiceGroupDto(
                    c.getLong(c.getColumnIndex(DbContract.TableServiceGroup._ID)),
                    c.getString(c.getColumnIndex(DbContract.TableServiceGroup.COLUMN_NAME_NAME)),
                    c.getString(c.getColumnIndex(DbContract.TableServiceGroup.COLUMN_NAME_DESCRIPTION))
                ));
            }
        }

        c.close();
        return groups;
    }
}
