package org.undp.ekyzmatker.activity;

import android.app.Application;
import android.content.Context;

import org.undp.ekyzmatker.dao.SQLiteDaoFactory;
import org.undp.ekyzmatker.dao.SQLiteServiceGroupDao;
import org.undp.ekyzmatker.entity.ServiceGroupDto;

/**
 * Created by aleksey on 01.06.15.
 */
public class AppMain extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        //KLUDGE
        AppMain.context = getApplicationContext();
        SQLiteDaoFactory daoFactory = new SQLiteDaoFactory(AppMain.getAppContext());

        daoFactory.openWritableConnection();
        SQLiteServiceGroupDao dao = (SQLiteServiceGroupDao)daoFactory.getServiceGroupDao();
        if (dao.findAll() == null)
            dao.insert(new ServiceGroupDto(
                1,
                "Здравоохранение",
                "Описание здравоохранения"
            ));

        daoFactory.closeConnection();
        //KLUDGE
    }

    public static Context getAppContext() {
        return AppMain.context;
    }
}
