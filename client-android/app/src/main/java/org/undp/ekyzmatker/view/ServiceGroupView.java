package org.undp.ekyzmatker.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.undp.ekyzmatker.R;
import org.undp.ekyzmatker.entity.ServiceGroupDto;
import org.undp.ekyzmatker.event.Event;
import org.undp.ekyzmatker.event.EventListener;
import org.undp.ekyzmatker.model.AppModel;

import java.util.ArrayList;

public class ServiceGroupView extends LinearLayout {

    private ViewListener mListener;
    private GridView mGrid;
    private AppModel mModel;
    private EventListener mEventListener = new EventListener() {
        @Override
        public void onEvent(Event event) {
            bind();
        }
    };

    public ServiceGroupView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mModel = AppModel.getInstance();
        }
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();

        mGrid = (GridView)findViewById(R.id.grid);
        mGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mListener.onItemClicked((ServiceGroupDto)parent.getItemAtPosition(position));
            }
        });

        if (!isInEditMode())
            bind();
    }

    public void setViewListener(ViewListener listener){
        mListener = listener;
    }

    private void bind() {
        GridAdapter adapter = new GridAdapter(getContext(), mModel.getServiceGroups());
        mGrid.setAdapter(adapter);
    }

    public interface ViewListener {
        void onItemClicked(ServiceGroupDto group);
    }

    private class GridAdapter extends ArrayAdapter<ServiceGroupDto> {

        private LayoutInflater mInflater;

        public GridAdapter(Context context, ArrayList<ServiceGroupDto> objects) {
            super(context, R.layout.item_service_group, objects);
            mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            ServiceGroupDto item = getItem(position);

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.item_service_group, parent, false);
                holder.image = (ImageView)convertView.findViewById(R.id.image);
                holder.text = (TextView)convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            holder.image.setImageDrawable(getResources().getDrawable(R.drawable.med_serv));
            holder.text.setText(item.getName());

            return convertView;
        }

        private class ViewHolder {
            ImageView image;
            TextView text;
        }
    }
}
