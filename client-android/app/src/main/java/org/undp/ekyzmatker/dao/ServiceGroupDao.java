package org.undp.ekyzmatker.dao;

import org.undp.ekyzmatker.entity.ServiceGroupDto;

import java.util.List;

/**
 * Created by aleksey on 28.05.15.
 */
public interface ServiceGroupDao {
    public long insert(ServiceGroupDto group);
    public boolean update(ServiceGroupDto group);
    public boolean delete(ServiceGroupDto group);
    public ServiceGroupDto findById(long id);
    public List<ServiceGroupDto> findAll();
}
