package org.undp.ekyzmatker.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.undp.ekyzmatker.R;
import org.undp.ekyzmatker.activity.AppMain;
import org.undp.ekyzmatker.entity.ServiceGroupDto;
import org.undp.ekyzmatker.model.AppModel;
import org.undp.ekyzmatker.view.ServiceGroupView;

/**
 * Created by aleksey on 01.06.15.
 */
public class ServiceGroupFragment extends Fragment {

    private AppModel mModel;
    private ServiceGroupView mView;
    private ServiceGroupView.ViewListener mViewListener = new ServiceGroupView.ViewListener() {
        @Override
        public void onItemClicked(ServiceGroupDto group) {
            Toast.makeText(AppMain.getAppContext(), "Group " + group.getName() + " selected", Toast.LENGTH_LONG).show();
        }
    };

    public ServiceGroupFragment() {
        super();
    }

    public static ServiceGroupFragment getInstance() {
        ServiceGroupFragment fragment = new ServiceGroupFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        mModel = AppModel.getInstance();
        mView = (ServiceGroupView)inflater.inflate(R.layout.layout_view_service_group, container, false);
        mView.setViewListener(mViewListener);

        return mView;
    }
}
