package org.undp.ekyzmatker.event;

/**
 * Created by aleksey on 28.05.15.
 */
public class SimpleEvent implements Event {

    private String mType;
    protected Object mSource;

    public SimpleEvent(String type) {
        mType = type;
    }

    @Override
    public String getType() {
        return mType;
    }

    @Override
    public Object getSource() {
        return mSource;
    }

    @Override
    public void setSource(Object source) {
        mSource = source;
    }
}
