package org.undp.ekyzmatker.event;

/**
 * Created by aleksey on 28.05.15.
 */
public interface Dispatcher {
    void addListener(String type, EventListener listener);
    void removeListener(String type, EventListener listener);
    boolean hasListener(String type, EventListener listener);
    void dispatchEvent(Event event);
}
