package org.undp.ekyzmatker.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.undp.ekyzmatker.database.DbContract;
import org.undp.ekyzmatker.database.DbHelper;

public class SQLiteDaoFactory extends DaoFactory {

    private static SQLiteDatabase mDatabase;
    private DbContract mDbContract;
    private DbHelper mDbHelper;
    private Context mContext;

    //Need revise
    public SQLiteDaoFactory(Context c) {
        mContext = c;
        mDbHelper = new DbHelper(mContext);
    }

    @Override
    public ServiceGroupDao getServiceGroupDao() {
        return new SQLiteServiceGroupDao(mDatabase);
    }

    @Override
    public void beginTransaction() {
        mDatabase.beginTransaction();
    }

    @Override
    public void endTransaction() {
        mDatabase.endTransaction();
    }

    @Override
    public void commitTransaction() {
        mDatabase.setTransactionSuccessful();
    }

    @Override
    public void openReadableConnection() {
        mDatabase = mDbHelper.getReadableDatabase();
    }

    @Override
    public void openWritableConnection() {
        mDatabase = mDbHelper.getWritableDatabase();
    }

    @Override
    public void closeConnection() {
        mDatabase.close();
    }
}
