package org.undp.ekyzmatker.activity;

import android.app.Activity;
import android.os.Bundle;

import org.undp.ekyzmatker.entity.ServiceGroupDto;
import org.undp.ekyzmatker.fragment.ServiceGroupFragment;
import org.undp.ekyzmatker.view.ServiceGroupView;


public class ServiceGroupActivity extends Activity {

    ServiceGroupView.ViewListener mViewListener = new ServiceGroupView.ViewListener() {

        @Override
        public void onItemClicked(ServiceGroupDto group) {

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        ServiceGroupFragment fragment = ServiceGroupFragment.getInstance();
        getFragmentManager().beginTransaction().add(android.R.id.content, fragment).commit();
    }

}