package org.undp.ekyzmatker.entity;

import android.os.Parcel;

/**
 * Created by aleksey on 28.05.15.
 */
public class ServiceGroupDto extends AbstractDto {
    private String mName;
    private String mDesc;

    public ServiceGroupDto(long id) {
        super(id);
    }

    public ServiceGroupDto(long id, String name, String desc) {
        super(id);
        mName = name;
        mDesc = desc;
    }

    public ServiceGroupDto(Parcel in) {
        super(in);
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    @Override
    public void readFromParcel(Parcel in) {
        setId(in.readLong());
        mName = in.readString();
        mDesc = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeLong(getId());
        out.writeString(mName);
        out.writeString(mDesc);
    }
}
