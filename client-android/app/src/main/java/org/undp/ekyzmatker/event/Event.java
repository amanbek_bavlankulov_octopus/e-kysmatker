package org.undp.ekyzmatker.event;

/**
 * Created by aleksey on 28.05.15.
 */
public interface Event {
    public String getType();
    public Object getSource();
    public void setSource(Object source);
}
