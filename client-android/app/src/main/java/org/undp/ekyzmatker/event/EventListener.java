package org.undp.ekyzmatker.event;

/**
 * Created by aleksey on 28.05.15.
 */
public interface EventListener {
    void onEvent(Event event);
}
