package org.undp.ekyzmatker.event;

import android.util.Log;

import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by aleksey on 28.05.15.
 */
public class EventDispatcher implements Dispatcher {
    private static final String TAG = EventDispatcher.class.getSimpleName();

    private HashMap<String, CopyOnWriteArrayList<EventListener>> mListenerMap;
    private Dispatcher mTarget;

    public EventDispatcher() {
        this(null);
    }

    public EventDispatcher(Dispatcher target) {
        mListenerMap = new HashMap <String, CopyOnWriteArrayList <EventListener>> ();
        mTarget = (mTarget != null) ? mTarget : this;
    }

    @Override
    public void addListener(String type, EventListener listener) {
        synchronized (mListenerMap) {
            CopyOnWriteArrayList <EventListener> list = mListenerMap.get(type);
            if (list == null) {
                list = new CopyOnWriteArrayList <EventListener> ();
                mListenerMap.put(type, list);
            }
            list.add(listener);
        }
    }

    @Override
    public void removeListener(String type, EventListener listener) {
        synchronized(mListenerMap) {
            CopyOnWriteArrayList <EventListener> list = mListenerMap.get(type);
            if (list == null)
                return;
            list.remove(listener);
            if(list.size() == 0) {
                mListenerMap.remove(type);
            }
        }
    }

    @Override
    public boolean hasListener(String type, EventListener listener) {
        synchronized (mListenerMap) {
            CopyOnWriteArrayList <EventListener> list = mListenerMap.get(type);
            if (list == null)
                return false;
            return list.contains(listener);
        }
    }

    @Override
    public void dispatchEvent(Event event) {
        if (event == null) {
            Log.e(TAG, "can not dispatch null event");
            return;
        }
        String type = event.getType();
        event.setSource(mTarget);
        CopyOnWriteArrayList<EventListener> list;
        synchronized(mListenerMap) {
            list = mListenerMap.get(type);
        }
        if (list == null)
            return;
        for (EventListener l : list) {
            l.onEvent(event);
        }
    }

    public void dispose() {
        synchronized(mListenerMap) {
            mListenerMap.clear();
        }
        mTarget = null;
    }
}
