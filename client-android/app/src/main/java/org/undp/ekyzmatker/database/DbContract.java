package org.undp.ekyzmatker.database;

import android.provider.BaseColumns;

public final class DbContract {
    public DbContract() {

    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    public static abstract class TableServiceGroup implements BaseColumns {
        public static final String TABLE_NAME = "t_service_group";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_DESCRIPTION = "desc";
    }

    public static final String SQL_CREATE_DB =
            "CREATE TABLE " + TableServiceGroup.TABLE_NAME + "(" +
            TableServiceGroup._ID + " INTEGER PRIMARY KEY," +
            TableServiceGroup.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
            TableServiceGroup.COLUMN_NAME_DESCRIPTION + TEXT_TYPE +
            ");";

    public static final String SQL_DELETE_DB =
            "DROP TABLE IF EXISTS " + TableServiceGroup.TABLE_NAME + ";";
}
