package org.undp.ekyzmatker.dao;

import org.undp.ekyzmatker.activity.AppMain;

/**
 * Created by aleksey on 19.05.15.
 */
public abstract class DaoFactory {
    public static final int SQLITE = 1;

    public abstract ServiceGroupDao getServiceGroupDao();

    public abstract void beginTransaction();
    public abstract void endTransaction();
    public abstract void commitTransaction();
    public abstract void openReadableConnection();
    public abstract void openWritableConnection();
    public abstract void closeConnection();

    public static DaoFactory getDaoFactory(int type) {
        switch(type) {
            case SQLITE:
                return new SQLiteDaoFactory(AppMain.getAppContext());
            default:
                return null;
        }
    }
}
