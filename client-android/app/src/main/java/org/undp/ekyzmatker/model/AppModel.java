package org.undp.ekyzmatker.model;

import org.undp.ekyzmatker.dao.DaoFactory;
import org.undp.ekyzmatker.dao.SQLiteDaoFactory;
import org.undp.ekyzmatker.dao.SQLiteServiceGroupDao;
import org.undp.ekyzmatker.entity.ServiceGroupDto;
import org.undp.ekyzmatker.event.EventDispatcher;
import org.undp.ekyzmatker.event.SimpleEvent;

import java.util.ArrayList;

/**
 * Created by aleksey on 19.05.15.
 */
public class AppModel extends EventDispatcher {

    private static AppModel mInstance;
    private ArrayList<ServiceGroupDto> mGroups;

    private AppModel() {
        //TODO
        SQLiteDaoFactory factory = (SQLiteDaoFactory) DaoFactory.getDaoFactory(DaoFactory.SQLITE);
        factory.openReadableConnection();
        SQLiteServiceGroupDao dao = (SQLiteServiceGroupDao)factory.getServiceGroupDao();
        mGroups = (ArrayList <ServiceGroupDto>)dao.findAll();
        factory.closeConnection();
    }

    public static AppModel getInstance() {
        if (mInstance == null) {
            mInstance = new AppModel();
        }

        return mInstance;
    }

    public static void destroyInstance() {
        if (mInstance != null) {
            mInstance = null;
        }
    }

    public ArrayList <ServiceGroupDto> getServiceGroups() {
        ArrayList <ServiceGroupDto> groups = new ArrayList(mGroups);
        return groups;
    }

    private void notifyChange(String type) {
        dispatchEvent(new ChangeEvent(type));
    }

    public static class ChangeEvent extends SimpleEvent {
        public static final String MODEL_CHANGED = "modelChanged";

        public ChangeEvent(String type) {
            super(type);
        }
    }
}
