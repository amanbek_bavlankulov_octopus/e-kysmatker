package org.undp.ekyzmatker.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aleksey on 19.05.15.
 */
public abstract class AbstractDto implements Parcelable {
    private long mId;
    public AbstractDto(long id) {
        mId = id;
    }

    public AbstractDto(Parcel in) {
        readFromParcel(in);
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public abstract void readFromParcel(Parcel in);
}
