﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content.Master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="WebEkyzmat.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <div class="portlet-title">
    <div class="caption">
      <%--<i class="fa fa-folder-open-o font-blue-sharp"></i>--%>
      <%--<a href="index.html">Назад на Главную</a>--%>
      <%--<span class="caption-subject font-green-sharp bold uppercase">--%>
      <span class="caption-subject font-blue-sharp bold">
        <%--<%= ((WebEkyzmat.Content)Page.Master).GetTitle(Request.QueryString["id"]) %>--%>
        <a href="Default.aspx">Назад на Главную</a>
      </span>
    </div>
    <div class="tools">
      <a class="fullscreen font-blue-sharp" href="javascript:;" data-original-title="" title=""></a>
      <%--<a href="javascript:;" class="collapse" data-original-title="" title=""></a>
      <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""></a>
      <a href="javascript:;" class="reload" data-original-title="" title=""></a>
      <a href="javascript:;" class="remove" data-original-title="" title=""></a>--%>
    </div>
  </div>

  <div class="portlet-body">

    <div class="row">
      <asp:Repeater ID="Repeater1" runat="server" EnableViewState="False">
        <ItemTemplate>

          <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat blue-madison" href='<%# GetUrl(Eval("id")) %>' runat="server">
              <%--<a href='<%# GetUrl(Eval("id")) %>' runat="server">--%>
                <div class="visual">
                  <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                  <div class="desc" style="font-size:26px;margin-left:18px;">
                    <%--New Feedbacks--%>
                    <asp:Literal runat="server" Text='<%# Eval("name")%>'></asp:Literal>
                  </div>
                  <%--<div class="number">
                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("id") %>'></asp:Literal>
                  </div>--%>
                </div>
              <%--</a>--%>
            </a>
            <span style="margin-bottom: 25px" />
          </div>
        </ItemTemplate>
      </asp:Repeater>
    </div>
  </div>

</asp:Content>
