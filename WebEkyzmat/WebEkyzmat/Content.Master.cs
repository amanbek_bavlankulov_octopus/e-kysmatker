﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebEkyzmat
{
  public partial class Content : System.Web.UI.MasterPage
  {
    public string MyDataSourceName { get { return XmlDataSource1.ID; } }
    public string MyProcName { get; set; }
    public string MyRouteName { get; set; }

    public string TitleName { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
      string id = Request.QueryString["id"];
      XmlDataSource1.Data = GetData(MyProcName, id);
      XmlDataSource1.DataBind();


      Session["routes"] = Session["routes"] + GetRoutes();

      ltrRoutes.Text = GetRoutes();//Session["routes"].ToString();
    }

    public string GetData(string procName, string id)
    {
      string ret = "<root/>";
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand(procName, conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@id", id);
        XmlReader reader = command.ExecuteXmlReader();//ExecuteReader();
        if (reader.Read())
          ret = reader.ReadOuterXml();
      }

      return ret;
    }

    protected string GetRoutes()
    {
      int id = int.Parse(Request.QueryString[0]);

      string titleName = "";

      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
      SqlConnection conn = new SqlConnection(connectionString);
      conn.Open();

      SqlCommand command = new SqlCommand("GetRoute", conn);
      command.CommandType = System.Data.CommandType.StoredProcedure;
      command.Parameters.AddWithValue("@type", MyRouteName);
      command.Parameters.AddWithValue("@id", id);
      SqlDataReader reader = command.ExecuteReader();

      string routeHtml = "";
      string pageName = ""; //Path.GetFileName(Request.CurrentExecutionFilePath.ToLower());
  
      List<Route> routes = new List<Route>();
      int count = 1;

      while (reader.Read())
      {
        if (count == 1)
          pageName = "Services.aspx";
        if (count == 2)
          pageName = "SubServices.aspx";
        if (count == 3)
          pageName = "Organizations.aspx";
        if (count == 4)
          pageName = "OrganizationInfo.aspx";

        routes.Add(new Route() { PageName = pageName, Id = reader.GetInt32(0), Name = reader.GetString(1) });
        count++;
      }

      for (int i = 0; i < routes.Count; i++)
      {
        if (i != routes.Count - 1)
        {
          routeHtml +=
                    string.Format("<li><a href=\"{0}?id={1}\">{2}</a><i class=\"fa fa-chevron-right\"></i></li>",
                    routes[i].PageName, routes[i].Id, routes[i].Name);
        }
        else // последний элемент
        {
          routeHtml += string.Format("<li class=\"active\">{0}</li>", routes[i].Name);
        }

      }


      TitleName = titleName;

      return routeHtml;
    }

    public string GetTitle(string id)
    {
      string ret = "";
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("dbo.GetTitle", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@type", MyRouteName);
        command.Parameters.AddWithValue("@id", id);
        SqlDataReader reader = command.ExecuteReader();
        if (reader.Read())
          ret = reader.IsDBNull(0) ? null : reader.GetString(0);
      }

      return ret;
    }

    #region История переходов
    //Ключ стека переходов в сессии	
    const String sHistoryStackName = "HistoryStack";

    /// <summary>
    /// Очистка истории переходов
    /// </summary>
    public void ResetHistory()
    {
      //ViewState.Remove(sHistoryStackName);
      Session.Remove(sHistoryStackName);
    }

    /// <summary>
    /// Имеется ли возможность возврата назад?
    /// </summary>
    public Boolean HasHistory
    {
      get
      {
        List<String> stckHistory = GetHistory();
        return stckHistory != null && stckHistory.Count > 0;
      }
    }

    /// <summary>
    /// Сохранение страницы для возврата
    /// </summary>
    /// <param name="sUrl"></param>
    public void SaveUrl(String sUrl)
    {
      List<String> stckHistory = GetHistory();
      if (stckHistory == null)
      {
        //ViewState[sHistoryStackName] = new List<String>();
        Session[sHistoryStackName] = new List<String>();
        stckHistory = GetHistory();
      }
      stckHistory.Add(sUrl);
    }

    /// <summary>
    /// Получение адреса на вершине истории
    /// </summary>
    public String TopUrl
    {
      get
      {
        List<String> stckHistory = GetHistory();
        if (stckHistory == null || stckHistory.Count <= 0)
          return null;

        return stckHistory[stckHistory.Count - 1];
      }
    }

    /// <summary>
    /// Раскрутка истории за указанную страницу,
    /// т.е. раскрутка до страницы и удаление сакмой страницы
    /// </summary>
    /// <param name="sPage"></param>
    public void UnwindPage(String sPageName)
    {
      List<String> stckHistory = GetHistory();
      if (stckHistory == null || stckHistory.Count <= 0)
        return;

      Int32 nFound = -1;
      for (Int32 i = stckHistory.Count - 1; i >= 0; i--)
      {
        if (stckHistory[i].Contains(sPageName))
        {
          nFound = i;
          break;
        }
      }

      if (nFound < 0)
        return;

      stckHistory.RemoveRange(nFound, stckHistory.Count - nFound);
    }

    /// <summary>
    /// Возврат назад
    /// </summary>
    /// <param name="response"></param>
    public void GoBack()
    {
      List<String> stckHistory = GetHistory();
      if (stckHistory == null || stckHistory.Count <= 0)
        return;


      String sUrl = stckHistory[stckHistory.Count - 1];
      stckHistory.RemoveAt(stckHistory.Count - 1);
      Response.Redirect(sUrl);
    }

    public List<String> GetHistory()
    {
      //return ViewState[sHistoryStackName] as List<String>;
      return Session[sHistoryStackName] as List<String>;
    }
    #endregion

    internal class Route
    {
      public string PageName { get; set; }
      public int Id { get; set; }
      public string Name { get; set; }
    }

  }
}