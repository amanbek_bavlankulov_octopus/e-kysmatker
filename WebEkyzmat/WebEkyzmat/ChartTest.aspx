﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChartTest.aspx.cs" Inherits="WebEkyzmat.ChartTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
  <script src="eKyzmatCharts.js" type="text/javascript"></script>
</head>
<body>
    <h4>eKyzmat Pie Chart</h4>
    <div id="eKyzmatPieChart" style="width: 900px; height: 500px;"></div>
</body>
</html>
