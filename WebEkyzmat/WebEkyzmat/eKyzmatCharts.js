﻿google.load("visualization", "1", { packages: ["corechart"] });
google.setOnLoadCallback(function () {

    //$(document).ready(function () {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            url: 'ChartHandler.ashx?chart=pie',
            success:
                function (response) {
                    //console.log(response);
                    drawChart(response);
                }
        });
    //})

});

function drawChart(dataValues) {
    var data = new google.visualization.DataTable();

    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');

    for (var i = 0; i < dataValues.length; i++) {
        //console.log(dataValues[i].Name);
        data.addRow([dataValues[i].Name, dataValues[i].Value]);
    }

    new google.visualization.PieChart(document.getElementById('eKyzmatPieChart')).draw(data, { title: "Условные данные" });
}


/*
var ChartsAmcharts = function () {
    alert('dsfsd');
    var initChartSample6 = function () {
        var chart = AmCharts.makeChart("eKyzmatPieChart", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',

            "color": '#888',

            "dataProvider": [{
                "country": "Lithuania",
                "litres": 501.9
            }, {
                "country": "Czech Republic",
                "litres": 301.9
            }, {
                "country": "Ireland",
                "litres": 201.1
            }, {
                "country": "Germany",
                "litres": 165.8
            }, {
                "country": "Australia",
                "litres": 139.9
            }, {
                "country": "Austria",
                "litres": 128.3
            }, {
                "country": "UK",
                "litres": 99
            }, {
                "country": "Belgium",
                "litres": 60
            }, {
                "country": "The Netherlands",
                "litres": 50
            }],
            "valueField": "litres",
            "titleField": "country",
            "exportConfig": {
                menuItems: [{
                    icon: Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                    format: 'png'
                }]
            }
        });

        $('#chart_6').closest('.portlet').find('.fullscreen').click(function () {
            chart.invalidateSize();
        });
    }
    return {
        //main function to initiate the module

        init: function () {

            //initChartSample1();
            //initChartSample2();
            //initChartSample3();
            //initChartSample4();
            //initChartSample5();
            initChartSample6();
            //initChartSample7();
            //initChartSample8();
            //initChartSample9();
            //initChartSample10();
            //initChartSample11();
            //initChartSample12();
        }

    };

}();
*/