﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;

namespace WebEkyzmat
{
  /// <summary>
  /// Summary description for ChartHandler
  /// </summary>
  public class ChartHandler : IHttpHandler
  {

    public void ProcessRequest(HttpContext context)
    {
      string ret = "";
      context.Response.ContentType = "text/plain";
      //context.Response.Write("Hello World");

      switch (context.Request["chart"])
      {
        case "pie":
          ret = GetPieData();
          break;

        default:
        ret = null;
          break;
      }


      context.Response.Write(ret);

    }

    public bool IsReusable
    {
      get
      {
        return false;
      }
    }


    string GetPieData()
    {
      string ret = "";

      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
      List<Pie> pieData = new List<Pie>();
      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("GetPieInfo", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        SqlDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
          pieData.Add(new Pie()
          {
            Name = reader.GetString(0),
            Value = reader.GetInt32(1)
          });
        }
      }


      string json = JsonConvert.SerializeObject(pieData);

      return json;
    }

    public class Pie
    {
      public string Name { get; set; }
      public int Value { get; set; }
    }

  }
}