﻿using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;

namespace WebEkyzmat
{
    /// <summary>
    /// Summary description for OpenDataHanlder
    /// </summary>
    public class OpenDataHanlder : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {

                var objectType = context.Request.QueryString["id"];
                var query = "GetOpendata";
                var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                var connection = new SqlConnection(connectionString);
                var command = new SqlCommand(query, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@id", objectType).Direction = ParameterDirection.Input;

                var csvContent = string.Empty;
                csvContent = "Название категории;";
                csvContent += "Описание категории;";
                csvContent += "Название направления услуги;";
                csvContent += "Описание направления услуги;";
                csvContent += "Название услуги;";
                csvContent += "Описание услуги;";
                csvContent += "Стандарт услуги;";
                csvContent += "Название организации;";
                csvContent += "Адрес;";
                csvContent += "Широта;";
                csvContent += "Долгота;";
                csvContent += "Описание;";
                csvContent += "Телефон;";
                csvContent += "Почта;";
                csvContent += "Веб-сайт;";
                csvContent += "Город;";
                csvContent += Environment.NewLine;

                var fileName = string.Empty;
                connection.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    fileName = reader.GetString(0);
                    csvContent += fileName + ";";
                    csvContent += reader.GetString(1) + ";";
                    csvContent += reader.GetString(2) + ";";
                    csvContent += reader.GetString(3) + ";";
                    csvContent += reader.GetString(4) + ";";
                    csvContent += reader.GetString(5) + ";";
                    csvContent += reader.GetString(6) + ";";
                    csvContent += reader.GetString(7) + ";";
                    csvContent += reader.GetString(8) + ";";
                    csvContent += reader.GetFloat(9) + ";";
                    csvContent += reader.GetFloat(10) + ";";
                    csvContent += reader.GetString(11) + ";";
                    csvContent += reader.GetString(12) + ";";
                    csvContent += reader.GetString(13) + ";";
                    csvContent += reader.GetString(14) + ";";
                    csvContent += reader.GetString(15) + ";" + Environment.NewLine;
                }

                connection.Close();

                context.Response.ContentType = "text/csv";
                //context.Response.ContentType = "application/vnd.ms-excel";
                var charset = Encoding.UTF8.WebName;
                context.Response.AddHeader("content-disposition", "attachment;filename=" + (string.IsNullOrEmpty(fileName) ? "NoData" : fileName.Replace(" ","_")  + ".csv"));
                context.Response.Charset = charset;
                context.Response.AddHeader("Pragma", "public");
                //context.Response.Write(csvContent);
                context.Response.BinaryWrite(Encoding.Default.GetBytes(csvContent));
            }
            catch (Exception ex)
            {
                context.Response.Write("Error");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}