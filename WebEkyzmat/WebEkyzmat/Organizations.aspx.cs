﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebEkyzmat
{
  public partial class Organizations : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Content masterPage = Page.Master as Content;
      masterPage.MyProcName = "FindOrganizations";
      masterPage.MyRouteName = "SubService";

      Repeater1.DataSourceID = masterPage.MyDataSourceName;

      srcTags.Data = GetTags();
      rptTags.DataSourceID = "srcTags";
      srcTags.DataBind();

      if (masterPage.HasHistory)
      {
        masterPage.UnwindPage("Organizations.aspx");
        HyperLink1.NavigateUrl = masterPage.TopUrl;
      }
      else
        HyperLink1.Visible = false;

      masterPage.SaveUrl(GetReturnUrl());
    }




    string GetTags()
    {
      string ret = "<root/>";
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("dbo.GetTags", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@SubserviceId", Request.QueryString["id"]);
        XmlReader reader = command.ExecuteXmlReader();//ExecuteReader();
        if (reader.Read())
          ret = reader.ReadOuterXml();
      }

      return ret;

    }

    string GetReturnUrl()
    {
      return "Organizations.aspx?id=" + Request.QueryString["id"];
    }

    public string GetUrl(object id)
    {
      return "OrganizationInfo.aspx?id=" + id;
    }

    public bool HasMapMarkers()
    {
      string json = GetOrganizationsMarkers(Request.QueryString["id"]);
      if (json == "[]")
        return false;

      return true;
    }


    [WebMethod]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public static string GetOrganizationsMarkers(object id)
    {
      string ret = "<root/>";
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
      List<OrganizationMarker> markers = new List<OrganizationMarker>();
      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("GetOrganizationsMarkers", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@id", id.ToString());
        SqlDataReader reader = command.ExecuteReader();//ExecuteReader();


        while (reader.Read())
        {
          float? latitude = reader.IsDBNull(0) ? null : (Nullable<float>)reader.GetFloat(0);
          float? longitude = reader.IsDBNull(1) ? null : (Nullable<float>)reader.GetFloat(1);
          string name = reader.IsDBNull(3) ? null : reader.GetString(3);
          string address = reader.IsDBNull(4) ? null : reader.GetString(4);

          if (latitude > 0
            && longitude > 0)
          {
            markers.Add(new OrganizationMarker()
            {
              Latitude = (float)latitude,
              Longitude = (float)longitude,
              Title = name,
              Address = address
            });

          }

        }
      }


      //XmlDocument xmlDoc = new XmlDocument();
      //xmlDoc.LoadXml(ret);

      //foreach (XmlNode item in xmlDoc.SelectNodes("root/item"))
      //{
      //  string latitude = item.SelectSingleNode("@latitude").InnerText;
      //  string longitude = item.SelectSingleNode("@longitude").InnerText;
      //  string name = item.SelectSingleNode("@name").InnerText;

      //  if (!string.IsNullOrEmpty(latitude)
      //    && !string.IsNullOrEmpty(longitude))
      //  {
      //    markers.Add(new OrganizationMarker()
      //    {
      //      Latitude = latitude,//float.Parse(latitude),
      //      Longitude = longitude,//float.Parse(longitude),
      //      Title = name
      //    });

      //  }

      //}

      //return ret;


      //markers.Add(new OrganizationMarker() { Latitude = 42.905309F, Longitude = 74.558477F, Title = "Title 1" });
      //markers.Add(new OrganizationMarker() { Latitude = 42.903547F, Longitude = 74.606936F, Title = "Title 2" });
      //markers.Add(new OrganizationMarker() { Latitude = 42.867050F, Longitude = 74.590809F, Title = "Title 3" });

      string json = JsonConvert.SerializeObject(markers);

      return json;
    }

    public class OrganizationMarker
    {
      public float Latitude { get; set; }
      public float Longitude { get; set; }
      public string Title { get; set; }
      public string Address { get; set; }
    }
  }
}