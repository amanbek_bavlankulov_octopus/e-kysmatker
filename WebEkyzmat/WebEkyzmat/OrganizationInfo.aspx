﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content.Master" AutoEventWireup="true" CodeBehind="OrganizationInfo.aspx.cs" Inherits="WebEkyzmat.OrganizationInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script src="http://maps.googleapis.com/maps/api/js"></script>

  <link href="assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen" />

  <script>

    setTimeout(function () {

      var organizationId = '<%= Request.QueryString["id"] %>';
      var ekyzmat_feedback_cookie = "ekyzmat_feedback_" + organizationId;

      if ($.cookie(ekyzmat_feedback_cookie) == null) {
        $("#feedback").show();
      } else {
        $("#youSendedFeedback").show();
      }

      $("#btnSubmitFeedback").click(function () {

        if (setFeedback()) {
          $("#feedback").hide();
          $("#thankForFeedback").show();
          $.cookie(ekyzmat_feedback_cookie, 1, { expires: 1 });
        }

      });

      $("#tbComment").focus(function () {
        $("#tbComment").prop("rows", 3);
      });

      $("#tbComment").focusout(function () {
        if ($("#tbComment").val().trim() == '')
          $("#tbComment").prop("rows", 1);
      });

      function setFeedback() {
        var feedback = true;
        var name = $("#fbackName").val();
        var phone = $("#fbackPhone").val();
        var comment = $("#tbComment").val();


        var fully = -1;
        if ($("#positiveFull").prop("checked"))
          fully = 1;
        if ($("#negativeFull").prop("checked"))
          fully = 0;
        //alert(fully);

        var timely = -1;
        if ($("#positiveTimely").prop("checked"))
          timely = 1;
        if ($("#negativeTimely").prop("checked"))
          timely = 0;
        //alert(timely);

        var kindly = -1;
        if ($("#positiveKindly").prop("checked"))
          kindly = 1;
        if ($("#negativeKindly").prop("checked"))
          kindly = 0;
        //alert(kindly);

        var discrimination = -1;
        if ($("#positiveDiscrimination").prop("checked"))
          discrimination = 1;
        if ($("#negativeDiscrimination").prop("checked"))
          discrimination = 0;
        //alert(discrimination);

        var sanitarly = -1;
        if ($("#positiveSanitarly").prop("checked"))
          sanitarly = 1;
        if ($("#negativeSanitarly").prop("checked"))
          sanitarly = 0;
        //alert(sanitarly);
        
        feedback = (fully != -1 || timely != -1 || kindly != -1 || discrimination != -1 || sanitarly != -1);
        

        if (feedback == false || name.trim() == '' || phone.trim() == '') {
          var message = "Для отправки Вашей оценки должны быть заполнены и выбраны следующие поля:\r\n"
            + (name.trim() == '' ? "- ФИО\r\n" : "")
            + (phone.trim() == '' ? "- Телефон\r\n" : "")
            + (feedback == false ? "- Один или более вариантов ответов\r\n" : "");
          //+"."
          alert(message);

          return false;
        }

        $.ajax({
          type: 'POST',
          url: 'OrganizationInfo.aspx/SetFeedback',
          contentType: "application/json; charset=utf-8",
          //dataType: "json",
          //data: '{}'
          data: "{'organizationId':'" + organizationId
            + "','name':'" + name
            + "','phone':'" + phone
            + "','fully':'" + fully
            + "','timely':'" + timely
            + "','kindly':'" + kindly
            + "','discrimination':'" + discrimination
            + "','sanitarly':'" + sanitarly
            + "','comment':'" + comment + "'}"
        });

        return true;
      }



      $('.fancybox').fancybox();

      // Disable opening and closing animations, change title type
      $(".fancybox-effects-b").fancybox({
        openEffect: 'none',
        closeEffect: 'none',

        //helpers: {
        //  title: {
        //    type: 'over'
        //  }
        //}
      });





    }, 400);

  </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="portlet-title">
    <div class="caption">
      <%--<i class="fa fa-folder-open-o font-blue-sharp"></i>--%>
      <%--<a href="index.html">Назад на Главную</a>--%>
      <%--<span class="caption-subject font-green-sharp bold uppercase">--%>


      <span class="caption-subject font-blue-sharp bold">

        <%--<a href="javascript:;" class="btn btn-link" title="Вернуться назад">
															<i class="fa fa-chevron-left"></i>
															</a>--%>

        <%--<%= ((WebEkyzmat.Content)Page.Master).GetTitle(Request.QueryString["id"]) %>--%>

        <asp:HyperLink ID="HyperLink1" runat="server">Вернуться назад</asp:HyperLink>
      </span>
    </div>
    <div class="tools">
      <a class="fullscreen font-blue-sharp" href="javascript:;" data-original-title="" title=""></a>
      <%--<a href="javascript:;" class="collapse" data-original-title="" title=""></a>
      <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""></a>
      <a href="javascript:;" class="reload" data-original-title="" title=""></a>
      <a href="javascript:;" class="remove" data-original-title="" title=""></a>--%>
    </div>
  </div>


  <div class="portlet-body">
    <%--<h4 class="block">
      <asp:HyperLink ID="HyperLink1" runat="server">Вернуться назад</asp:HyperLink>
    </h4>--%>

    <asp:FormView ID="FormView1" runat="server" Width="100%">
      <ItemTemplate>
        <div class="row" onload="showFeedback(<%# Eval("display_feedback")%>);">
          <div class="col-md-3">
            <center>
            <a class="fancybox-effects-b" href='<%# Eval("photo_name") %>' title="">
              <img src='<%# Eval("photo_name") %>' height="250px" alt="" class="img-thumbnail">
            </a>
            </center>
            <%--<br />
            <br />
            <ul class="ver-inline-menu tabbable margin-bottom-10">
              <li class="active">
                <a data-toggle="tab" href="#tab_1-1" aria-expanded="true">
                  <i class="fa fa-info-circle"></i>Общее описание</a>
                <span class="after"></span>
              </li>
              <li class="">
                <a data-toggle="tab" href="#tab_2-2" aria-expanded="false" onclick="resizeMap()">
                  <i class="fa fa-search"></i>Посмотреть на карте</a>
              </li>
            </ul>--%>
          </div>




          <div class="col-md-<%# (Eval("display_feedback").ToString() == "1" ? 6 : 9)%>">
            <h2>
              <%# Eval("name") %>
            </h2>
            <p>
              <%# Eval("desc") %>
            </p>
            <p>
              <%# GetPhones(Eval("tel_no")) %>
              <%# Eval("address") != "" ? "<br/><b>Адрес:</b>&nbsp;": "" %> <%#Eval("address") %>
              <%# Eval("web_site") != "" ? "<br/><b>Веб-сайт:</b>&nbsp;": "" %><a href='<%# Eval("web_site") %>' target="_blank"><%# Eval("web_site") %></a>
              <%# Eval("download_link") != "" ? "<br/><b>Скачать:</b>&nbsp;": "" %><a href='<%# Eval("download_link") %>' target="_blank"><%# Eval("download_link") %></a>
            </p>

            <script type="text/javascript">
              var map;

              function initialize() {
                var myLatlng = new google.maps.LatLng('<%# Eval("latitude") %>', '<%# Eval("longitude") %>');
                var mapProp = {
                  center: myLatlng,
                  zoom: 15,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("gmap-polylines"), mapProp);

                var marker = new google.maps.Marker({
                  position: myLatlng,
                  title: '<%# Eval("name") %>',
                  icon: 'img/male-copy.png'
                });

                marker.setMap(map);

                var markerContent = '<b>'.concat('<%# Eval("name") %>').concat('</b><br/>').concat('<%# Eval("address") %>');
                var iw = new google.maps.InfoWindow({ content: markerContent });
                google.maps.event.addListener(marker, "click", function (e) { iw.open(map, marker); });

                //$("#map_content").show();
              };

              google.maps.event.addDomListener(window, 'load', initialize);
              google.maps.event.addDomListener(window, "resize", resizingMap());

              function resizeMap() {
                if (typeof map == "undefined") return;
                setTimeout(function () { resizingMap(); }, 400);
              }

              function resizingMap() {
                if (typeof map == "undefined")
                  return;
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
              }
            </script>

            <div id="map_content" <%# (Eval("latitude").ToString() == "0" || Eval("longitude").ToString() == "0")? "style=\"display:none;\"":"style=\"\"" %>>
              <h4>Карта</h4>
              <div id="gmap-polylines" class="gmaps">
              </div>
            </div>
          </div>

          <div class="col-md-<%# (Eval("display_feedback").ToString() == "1" ? 3:0)%>" <%# (Eval("display_feedback").ToString() == "1" ? "": "style=\"display:none;\"")%>>
            <div id="youSendedFeedback" style="display: none;">
              <h4>Вы уже оставили оценку
              </h4>
            </div>
            <div id="thankForFeedback" style="display: none;">
              <h4>Спасибо за Вашу оценку!
              </h4>
            </div>
            <div id="feedback" style="display: none;">
              <h4>Оцените качество предоставляемых услуг данным органом
              </h4>
              <div class="form-group">
                <div class="input-icon">
                  <i class="fa fa-user"></i>
                  <input class="form-control" id="fbackName" placeholder="ФИО">
                </div>
              </div>
              <div class="form-group">
                <div class="input-icon">
                  <i class="fa fa-phone"></i>
                  <input class="form-control" id="fbackPhone" placeholder="Телефон">
                </div>
              </div>
              <%--<label>Оценка:</label>--%>
              <div id="radioItems">
                <ul class="media-list">
                  <li class="media">
                    <i class="fa fa-search pull-left"></i>
                    <div class="media-body">
                      Услуга была получена в полном объеме<br>
                      <label class="radio-inline">
                        <input id="positiveFull" type="radio" name="one">Да, была</label>
                      <label class="radio-inline">
                        <input id="negativeFull" type="radio" name="one">Нет, не была</label>
                    </div>
                  </li>
                  <li class="media">
                    <i class="fa fa-clock-o pull-left"></i>
                    <div class="media-body">
                      Услуга была предоставлена своевременно<br>
                      <label class="radio-inline">
                        <input id="positiveTimely" type="radio" name="two">Да, была</label>
                      <label class="radio-inline">
                        <input id="negativeTimely" type="radio" name="two">Нет, не была
                      </label>
                    </div>
                  </li>
                  <li class="media">

                    <i class="fa fa-users pull-left"></i>
                    <div class="media-body">
                      Отношение сотрудников было доброжелательным<br>
                      <label class="radio-inline">
                        <input id="positiveKindly" type="radio" name="three">Да, было
                      </label>
                      <label class="radio-inline">
                        <input id="negativeKindly" type="radio" name="three">Нет, не было
                      </label>
                    </div>
                  </li>
                  <li class="media">

                    <i class="fa fa-comments-o pull-left"></i>
                    <div class="media-body">
                      Со стороны сотрудников была дискриминация<br>
                      <label class="radio-inline">
                        <input id="positiveDiscrimination" type="radio" name="four">Да, была
                      </label>
                      <label class="radio-inline">
                        <input id="negativeDiscrimination" type="radio" name="four">Нет, не была</label>
                    </div>
                  </li>
                  <li class="media">
                    <i class="fa fa-institution pull-left"></i>
                    <div class="media-body">
                      Помещение госучреждения соответствует санитарным нормам<br>
                      <label class="radio-inline">
                        <input id="positiveSanitarly" type="radio" name="five">Да, соответствует</label>
                      <label class="radio-inline">
                        <input id="negativeSanitarly" type="radio" name="five">Нет, не соответствует</label><br />
                    </div>
                  </li>

                </ul>
                <%--<label class="radio-inline">
                  <input id="rbExellent" type="radio" name="optradio">Да<input id="Radio1" type="radio" name="optradio">Нет</label><br />
                <br />
                <label class="radio-inline">
                  <input id="rbGood" type="radio" name="optradio">&nbsp;Услуга была предоставлена своевременно</label><br />
                <br />
                <label class="radio-inline">
                  <input id="rbMiddle" type="radio" name="optradio">&nbsp;Отношение сотрудников было доброжелательным</label><br />
                <br />
                <label class="radio-inline">
                  <input id="rbBad" type="radio" name="optradio">&nbsp;Со стороны сотрудников не было дискриминации</label><br />
                <br />
                <label class="radio-inline">
                  <input id="rbTerrible" type="radio" name="optradio">&nbsp;Помещение госучреждения соответствует санитарным нормам</label><br />
                <br />--%>
              </div>

              <div class="form-group">
                <label>Ваш отзыв:</label>
                <textarea id="tbComment" class="form-control" rows="1"></textarea>
              </div>
              <a id="btnSubmitFeedback" href="javascript:;" data-toggle="modal" class="btn btn-sm default" style="">Отправить
              </a>
            </div>
          </div>

        </div>

      </ItemTemplate>
    </asp:FormView>
</asp:Content>
