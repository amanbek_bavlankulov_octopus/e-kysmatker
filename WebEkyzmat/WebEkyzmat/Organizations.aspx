﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content.Master" AutoEventWireup="true" CodeBehind="Organizations.aspx.cs" Inherits="WebEkyzmat.Organizations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script src="http://maps.googleapis.com/maps/api/js"></script>
  <script type="text/javascript">

    var map;
    //var center_position = { lat: 42.871017, lng: 74.583398 };
    //Kyrgyzstan 41.606492, 74.808694
    var center_position = { lat: 41.606492, lng: 74.808694 };

    function initMap() {
      // Create a map object and specify the DOM element for display.
      map = new google.maps.Map(document.getElementById('map_organizations'), {
        center: center_position,
        scrollwheel: true,
        zoom: 10
      });
      var iw;

      setTimeout(function () {
        $.ajax({
          type: 'GET',
          dataType: 'json',
          contentType: 'application/json',
          url: 'Organizations.aspx/GetOrganizationsMarkers',
          data: 'id=<%= Request.QueryString["id"] %>',
          success:
              function (response) {

                //console.log(response.d);
                var markers = $.parseJSON(response.d);

                //console.log(markers);

                for (var i = 0; i < markers.length; i++) {

                  //var pointMarkerImage[i] = new google.maps.MarkerImage('marker.png');
                  var pointMarker = new google.maps.Marker({
                    position: { lat: markers[i].Latitude, lng: markers[i].Longitude },
                    map: map,
                    icon: 'img/reception.png',
                    //animation: google.maps.Animation.BOUNCE,
                    title: markers[i].Title,
                    //content: '<b>'.concat(markers[i].Title).concat('</b><br/>').concat(markers[i].Address)
                  });


                  //google.maps.event.addListener(pointMarker, 'click', function () {
                  //  infowindow.setContent("We can include any station information, for example: Lat/Long: " + stations[1] + " , " + stations[2]);
                  //  var markerContent = '<b>'.concat(markers[i].Title).concat('</b><br/>').concat(markers[i].Address);
                  //  var infowindow = new google.maps.InfoWindow({
                  //    content: markerContent
                  //  });
                    
                  //  infowindow.open(map, pointMarker);
                  //});


                }

              }
        });

      }, 1000);


    }

    google.maps.event.addDomListener(window, 'load', initMap);
    google.maps.event.addDomListener(window, "resize", resizingMap());

    //$('#map').on('onresize', function () {
    //  //Must wait until the render of the modal appear, thats why we use the resizeMap and NOT resizingMap!! ;-)
    //  resizeMap();
    //})


    function resizeMap() {
      if (typeof map == "undefined") return;
      setTimeout(function () { resizingMap(); }, 400);
    }

    function resizingMap() {
      if (typeof map == "undefined") return;
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    }

    // Фильтрация данных
    function setFilter(element) {
      var a = $(element).data('filter');

      $("#tag_filter > li").removeClass('active');
      $(element).parent().addClass('active');

      $('.col-lg-4').hide();
      $("div[class*='" + a + "']").show();
    }

    // Отображаем фильтр если значение тегов > 1
    setTimeout(function () {
      if ($("#tag_filter li").length > 1) {
        $("#tag_filter").css('display', '');//hide();//.removeAttr("display");
      }
    }, 500);

  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="portlet-title">
    <div class="caption">
      <%--<i class="fa fa-folder-open-o font-blue-sharp"></i>--%>
      <%--<a href="index.html">Назад на Главную</a>--%>
      <%--<span class="caption-subject font-green-sharp bold uppercase">--%>
      <span class="caption-subject font-blue-sharp bold">
        <%--<%= ((WebEkyzmat.Content)Page.Master).GetTitle(Request.QueryString["id"]) %>--%>
        <asp:HyperLink ID="HyperLink1" runat="server">Вернуться назад</asp:HyperLink>
      </span>

    </div>

    <div class="tools pull-right">
      <%--<a href="javascript:;" class="fa fa-globe font-blue-sharp" >--%>

      <%--<i class="fa fa-globe"></i>--%>
      <a class="fullscreen font-blue-sharp" href="javascript:;" data-original-title="" title=""></a>
      <%--<a href="javascript:;" class="collapse" data-original-title="" title=""></a>
      <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""></a>
      <a href="javascript:;" class="reload" data-original-title="" title=""></a>
      <a href="javascript:;" class="remove" data-original-title="" title=""></a>--%>
    </div>
    <div class="actions">

      <% if (HasMapMarkers())
         {%>

      <a href="#map" data-toggle="modal" class="btn btn-sm default" style="">Посмотреть на карте <i class="fa fa-globe"></i>
      </a>

      <%} %>
    </div>
  </div>


  <div class="portlet-body">

    <asp:XmlDataSource runat="server" ID="srcTags" EnableCaching="false" XPath="root/item" />
    <asp:Repeater runat="server" ID="rptTags">
      <HeaderTemplate>
        <ul class="nav nav-pills" id="tag_filter" style="display:none;">
          <li class="active"><a href="javascript:void(0);" data-filter="all" onclick="setFilter(this);">Все</a></li>
      </HeaderTemplate>
      <ItemTemplate>
        <li><a href="javascript:void(0);" data-filter="<%# Eval("tag") %>" onclick="setFilter(this);"><%# Eval("tag") %></a></li>
      </ItemTemplate>
      <FooterTemplate>
        </ul>
      </FooterTemplate>
    </asp:Repeater>

    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="False">
      <HeaderTemplate>
        <div class="row">
      </HeaderTemplate>
      <ItemTemplate>

        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 all <%# Eval("tag") %>">
          <a class="dashboard-stat blue-madison" href='<%# GetUrl(Eval("id")) %>' runat="server">
            <div class="visual">
              <i class="fa fa-graduation-cap"></i>
            </div>
            <div class="details">
              <div class="desc" style="font-size: 18px; margin-left: 18px;">
                <%# Eval("name")%>
              </div>
              <%--<div class="number">
                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("id") %>'></asp:Literal>
                  </div>--%>
            </div>
            <div class="more">
              <em>
                <i class="fa fa-tags"></i>
                <%# Eval("tag") %>
              </em>
            </div>
          </a>
          <span style="margin-bottom: 25px" />
        </div>


      </ItemTemplate>
      <FooterTemplate>
        </div>
        <div class="modal fade bs-modal-lg" id="map" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" onfocus="resizeMap()">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title bold">Расположение субъектов на карте
                </h4>
              </div>
              <div class="modal-body" style="height: 100%; overflow-y: auto; width: 100%;">
                <%--<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>--%>

                <div id="map_organizations" style="width: 100%; height: 400px; margin: 0 auto;"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Закрыть</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

      </FooterTemplate>
    </asp:Repeater>
  </div>
</asp:Content>
