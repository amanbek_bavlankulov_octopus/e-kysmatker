﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
//using WebEkyzmat.Objects;

namespace WebEkyzmat
{
  public partial class Data : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      string data = "<root/>";

      // Главная - это условие никогда не исполняется
      string serviceGroup = Request.QueryString["ServiceGroup"];
      if (!string.IsNullOrEmpty(serviceGroup))
      {
        data = GetData("FindCategories", serviceGroup);
        Session["type"] = "Service";
        
      }
      //
      string service = Request.QueryString["Service"];
      if (!string.IsNullOrEmpty(service))
      {
        data = GetData("FindServices", service);
        Session["type"] = "SubService";
        //lbInfo.Text = "Cудебная система";
      }

      string subService = Request.QueryString["SubService"];
      if (!string.IsNullOrEmpty(subService))
      {
          data = GetData("FindSubServices", subService);
          Session["type"] = "Organization";
          //lbInfo.Text = "Услуги";
      }

      string findOrganizations = Request.QueryString["Organization"];
      if (!string.IsNullOrEmpty(findOrganizations))
      {
          data = GetData("FindOrganizations", findOrganizations);
          Session["type"] = "OrganizationInfo";
          //lbInfo.Text = "Организации";
      }

      string findOrganizationInfo = Request.QueryString["OrganizationInfo"];
      if (!string.IsNullOrEmpty(findOrganizationInfo))
      {
          data = GetData("FindOrganizationInfo", findOrganizationInfo);
          var doc = new XmlDocument();
          doc.LoadXml(data);

          var selectSingleNode = doc.SelectSingleNode("root");
          var name = selectSingleNode.SelectSingleNode("item").Attributes["name"].Value;
          var desc = selectSingleNode.SelectSingleNode("item").Attributes["desc"].Value;
          var address = selectSingleNode.SelectSingleNode("item").Attributes["address"].Value;
          var tel = selectSingleNode.SelectSingleNode("item").Attributes["tel_no"].Value;

          Label2.Text = "<b>Наименование : " + name + "<br/>Описание : " + desc + "<br/>" + "Адрес : " + address + "<br/>" + "Телефон : " + tel + "</b>";
          Session["type"] = null;
          lbInfo.Text = "Организация";
          return;
      }


      XmlDataSource1.Data = data;
      Repeater1.DataSourceID = "XmlDataSource1";
      XmlDataSource1.DataBind();
      Repeater1.DataBind();
    }


    string GetData(string procName, string id)
    {
      string ret = "<root/>";
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      SqlConnection conn = new SqlConnection(connectionString);
      conn.Open();
      SqlCommand command = new SqlCommand(procName, conn);
      command.CommandType = System.Data.CommandType.StoredProcedure;
      command.Parameters.AddWithValue("@id", id);
      SqlDataReader reader = command.ExecuteReader();

      while (reader.Read())
      {
        ret = reader.IsDBNull(0) ? "<root/>" : reader.GetString(0);
      }
      return ret;
    }
    
    public string GetUrl(object id)
    {
      string type = (string)Session["type"];

      if (String.IsNullOrEmpty(type))
        return "#";

      return string.Format("~/Data.aspx?{0}={1}", Session["type"], id);
    }

    protected string GetRoutes()
    {
      string levelName = (string)Session["type"];
      int id = int.Parse(Request.QueryString[levelName]);

      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
      SqlConnection conn = new SqlConnection(connectionString);
      conn.Open();

      SqlCommand command = new SqlCommand("GetRoute", conn);
      command.CommandType = System.Data.CommandType.StoredProcedure;
      command.Parameters.AddWithValue("@type", levelName);
      command.Parameters.AddWithValue("@id", id);
      SqlDataReader reader = command.ExecuteReader();

      string routeHtml = "";

      while (reader.Read())
      {
        var loop = true;
        while (loop)
        {
          //1. Here retrive values you need e.g. var myvar = reader.GetBoolean(0);
          loop = reader.Read();
          if (!loop)
          {
            //You are on the last record. Use values read in 1.
            //Do some exceptions
            routeHtml += routeHtml
              = string.Format("<li><a href=\"Data.aspx?{0}={1}\">{2}</a></li>",
                levelName, id, reader.GetString(1));
          }
          else
          {
            //You are not on the last record.
            //Process values read in 1., e.g. myvar
            routeHtml += routeHtml
              = string.Format("<li><a href=\"Data.aspx?{0}={1}\">{2}</a><i class=\"fa fa-circle\"></i></li>",
                levelName, id, reader.GetString(1));
          }
        }
  
      }

      return routeHtml;

      //<li>
      //      <a href=""#"">Судебная система</a>
      //      <!--i class=""fa fa-circle""></i-->
      //</li>
    }

    //Category GetCategory(int id)
    //{
    //  string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
    //  SqlConnection conn = new SqlConnection(connectionString);
    //  conn.Open();
    //  Category category = new Category();

    //  SqlCommand command = new SqlCommand("dbo.GetCategory", conn);
    //  SqlDataReader reader = command.ExecuteReader();
    //  while (reader.Read())
    //  {
    //      category.Id = reader.GetInt32(0);
    //      category.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
    //      category.Description = reader.IsDBNull(2) ? null : reader.GetString(2);
    //      category.Icon = reader.IsDBNull(3) ? null : reader.GetString(3);
    //  }

    //  return category;
    //}



    



  }
}