﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebEkyzmat
{
  public partial class Services : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Content masterPage = Page.Master as Content;
      masterPage.MyProcName = "FindServices";
      masterPage.MyRouteName = "ServiceGroup";

      Repeater1.DataSourceID = masterPage.MyDataSourceName;


      masterPage.ResetHistory();
      masterPage.SaveUrl(GetReturnUrl());
    }

    string GetReturnUrl()
    {
      return "Services.aspx?id=" + Request.QueryString["id"];
    }

    public string GetUrl(object id)
    {
      return "SubServices.aspx?id=" + id;
    }
  }
}