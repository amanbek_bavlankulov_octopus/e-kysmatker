﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Content.Master" AutoEventWireup="true" CodeBehind="SubServices.aspx.cs" Inherits="WebEkyzmat.SubServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="portlet-title">
    <div class="caption">
      <%--<i class="fa fa-folder-open-o font-blue-sharp"></i>--%>
      <%--<a href="index.html">Назад на Главную</a>--%>
      <%--<span class="caption-subject font-green-sharp bold uppercase">--%>
      <span class="caption-subject font-blue-sharp bold">
        <%--<%= ((WebEkyzmat.Content)Page.Master).GetTitle(Request.QueryString["id"]) %>--%>
        <asp:HyperLink ID="HyperLink1" runat="server">Вернуться назад</asp:HyperLink>
      </span>
    </div>
    <div class="tools">
      <a class="fullscreen font-blue-sharp" href="javascript:;" data-original-title="" title=""></a>
      <%--<a href="javascript:;" class="collapse" data-original-title="" title=""></a>
      <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""></a>
      <a href="javascript:;" class="reload" data-original-title="" title=""></a>
      <a href="javascript:;" class="remove" data-original-title="" title=""></a>--%>
    </div>
  </div>

  <div class="portlet-body">
    <%-- <h4 class="block">
      <asp:HyperLink ID="HyperLink1" runat="server">Вернуться назад</asp:HyperLink>
    </h4>--%>
    <div class="row">
      <asp:Repeater ID="Repeater1" runat="server" EnableViewState="False">
        <ItemTemplate>


          <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue-madison">
              <a href='<%# GetUrl(Eval("id")) %>' runat="server">
                <div class="visual">
                  <i class="fa fa-institution"></i>
                </div>
                <div class="details">
                  <%--<div class="number"></div>--%>
                  <div class="desc" style="font-size: 18px; margin-left: 18px;">
                    <%--New Feedbacks--%>
                    <%# Eval("name")%>
                  </div>
                </div>
              </a>
              <a class="more" data-toggle="modal" href='<%# GetDialogLink(Eval("id")) %>' style="padding-top: 10px; padding-bottom: 15px;">
                <span style="font-size: 14px;">Информация</span>
                <%--<i class="m-icon-big-swapright m-icon-white"></i>--%>
              </a>

            </div>
            <span style="margin-bottom: 25px" />
          </div>

        </ItemTemplate>
      </asp:Repeater>
    </div>
  </div>






















  <asp:Repeater ID="Repeater2" runat="server" EnableViewState="False">
    <ItemTemplate>
      <div class="modal fade bs-modal-lg" id='<%# BuildDialogId(Eval("id")) %>' tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title bold">
                <asp:Literal Text='<%# Eval("Name") %>' runat="server" />
              </h4>
            </div>
            <div class="modal-body" style="max-height: calc(100vh - 200px); overflow-y: auto;">
              <asp:Literal ID="Literal1" Text='<%# Eval("guide") %>' runat="server" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn default" data-dismiss="modal">Закрыть</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </ItemTemplate>
  </asp:Repeater>
</asp:Content>
