﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebEkyzmat
{
  public partial class ChartTest : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      //string xml = "<root><item name=\"name1\" value=\"value1\"/><item name=\"name2\" value=\"value2\"/><item name=\"name3\" value=\"value3\"/></root>";

      //XmlDocument xmlDoc = new XmlDocument();
      //xmlDoc.LoadXml(xml);


      //string json = GetPieData();

      //Response.Write(json);
    }

    string GetPieData()
    {
      string ret = "";

      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("GetPieInfo", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        XmlReader reader = command.ExecuteXmlReader();
        if (reader.Read())
          ret = reader.ReadOuterXml();
      }

       XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(ret);


      string json = JsonConvert.SerializeXmlNode(xmlDoc);





      return json;
    }

  }
}