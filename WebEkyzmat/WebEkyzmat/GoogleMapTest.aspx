﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GoogleMapTest.aspx.cs" Inherits="WebEkyzmat.GoogleMapTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <script src="http://maps.googleapis.com/maps/api/js"></script>
  <script type="text/javascript">
    function initMap() {
      var myLatLng = { lat: 42.8757537, lng: 74.5715228 };

      // Create a map object and specify the DOM element for display.
      var map = new google.maps.Map(document.getElementById('map_organizations'), {
        center: myLatLng,
        scrollwheel: true,
        zoom: 12
      });
/*
      // Create a marker and set its position.
      var marker = new google.maps.Marker({
        map: map,
        position: myLatLng,
        title: 'Hello World!'
      });

      var markerArray = {}


      for (var i = 0; i < markerArray.length; i++) {
        setTimeout(function () {
          addMarkerMethod();
        }, i * 200);
      }

      */

      var collection = new Array();

      //a set of locations stored in array
      collection[0] = new google.maps.LatLng(42.905309, 74.558477);
      collection[1] = new google.maps.LatLng(42.903547, 74.606936);
      collection[2] = new google.maps.LatLng(42.867050, 74.590809);

      //var pointMarkerImage = new Array();//store image of marker in array
      //var pointMarker = new Array();//store marker in array

      //create number of markers based on collection.length
      //function setPoint(){
        for(var i=0; i<collection.length; i++){

          //var pointMarkerImage[i] = new google.maps.MarkerImage('marker.png');
           var pointMarker = new google.maps.Marker({
            position: collection[i],
            map: map,
            //icon: pointMarkerImage[i],
            //animation: google.maps.Animation.BOUNCE,
            title: "collection"+ i 
          });
        }
      //}


    }
    google.maps.event.addDomListener(window, 'load', initMap);
  </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="map" style="width:800px;height:600px;">
    
    </div>
    </form>
</body>
</html>
