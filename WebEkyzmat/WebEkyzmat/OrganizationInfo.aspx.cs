﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebEkyzmat
{
  public partial class OrganizationInfo : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Content masterPage = Page.Master as Content;
      masterPage.MyProcName = "FindOrganizationInfo";
      masterPage.MyRouteName = "Organization";
      FormView1.DataSourceID = masterPage.MyDataSourceName;

      if (masterPage.HasHistory)
      {
        masterPage.UnwindPage("OrganizationInfo.aspx");
        HyperLink1.NavigateUrl = masterPage.TopUrl;
      }
      else
        HyperLink1.Visible = false;

    }

    //public bool HasMapPosition(object latitude, object longitude)
    //{
    //  return false;
    //}

    public string GetDescription(object xml)
    {
      var xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(xml.ToString());

      var root = xmlDoc.SelectSingleNode("root");
      var nodes = root.SelectNodes("item");
      var text = string.Empty;

      foreach (XmlNode node in nodes)
      {
        text += node.InnerText + "<br/>";
      }
      return text;
    }

    protected string GetPhones(object xml)
    {
      string html = "";// xml.ToString();
      try
      {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xml.ToString());

        foreach (XmlNode item in xmlDoc.SelectNodes("/root/phone"))
        {
          string captionAndPhone = "<b>{0}:</b>&nbsp;<a href=\"tel:{1}\">{1}</a><br/>";

          captionAndPhone = string.Format(captionAndPhone, item.SelectSingleNode("@caption").InnerText, item.SelectSingleNode("@number").InnerText);
          html += captionAndPhone;
        }
      }
      catch (Exception)
      { }

      return html;
    }


    [WebMethod]
    //[ScriptMethod(UseHttpGet = true)]
    public static void SetFeedback(string organizationId, string name, string phone, string fully, string timely, string kindly, string discrimination, string sanitarly, string comment)
    {
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("dbo.SetFeedback", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        //command.Parameters.AddWithValue("@FeedbackId", feedback);
        command.Parameters.AddWithValue("@OrganizationId", organizationId);
        command.Parameters.AddWithValue("@Name", name);
        command.Parameters.AddWithValue("@Phone", phone);
        command.Parameters.AddWithValue("@Fully",(string.IsNullOrEmpty(fully)|| fully == "null")? -1: Convert.ToInt32(fully));
        command.Parameters.AddWithValue("@Timely", (string.IsNullOrEmpty(timely) || timely == "null") ? -1 : Convert.ToInt32(timely));
        command.Parameters.AddWithValue("@Kindly", (string.IsNullOrEmpty(kindly) || kindly == "null") ? -1 : Convert.ToInt32(kindly));
        command.Parameters.AddWithValue("@Discrimination", (string.IsNullOrEmpty(discrimination) || discrimination == "null") ? -1 : Convert.ToInt32(discrimination));
        command.Parameters.AddWithValue("@Sanitarly", (string.IsNullOrEmpty(sanitarly) || sanitarly == "null") ? -1 : Convert.ToInt32(sanitarly));
        command.Parameters.AddWithValue("@Comment", comment);
        command.ExecuteNonQuery();

      }
    }

  }
}