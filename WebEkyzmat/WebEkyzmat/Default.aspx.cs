﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebEkyzmat
{
  public partial class Default : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

      srcCategories.Data = GetCategories();
      srcCategories.DataBind();
      rptCategories.DataSourceID = srcCategories.ID;
    }


    string GetCategories()
    {
      string ret = "<root/>";
      string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

      using (SqlConnection conn = new SqlConnection(connectionString))
      {
        conn.Open();
        SqlCommand command = new SqlCommand("FindCategories", conn);
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@id", 0);
        XmlReader reader = command.ExecuteXmlReader();//ExecuteReader();
        if (reader.Read())
          ret = reader.ReadOuterXml();
      }

      return ret;
    }

    // формируем описание с дополнительными недостающими элементами <br/>
    protected string GetDesc(object obj)
    {
      string desc = obj.ToString();

      // допустимо всего 2 элемента <br/>
      int brCount = 2 - Regex.Matches(desc, "<br/>").Count;
      string brLiterals = "";
      for (; brCount > 0; --brCount)
      {
        brLiterals += "<br/>&nbsp;";
      }

      return desc + brLiterals;
    }


    // количество колонок на странице для отображения списка категорий
    // в случае изменения кол-во колонок следует также поменять значения col-md-X (нужно поменять значение X)
    // в html контенте, согласно документации bootstrap
    int col = 4;

    protected string GetItemLiteral(object obj)
    {
      int index = Convert.ToInt32(obj);

      if (index % col != 0)
        return "item";

      return "";
    }

    // Для вставки элемента <div class=\"row\"> в начало строки данных 
    // содержащее колонки размером col (смотрите перменную col)
    /*
     * Пример:
     <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 item">   -Содержимое-   </div>
            <div class="col-md-3 col-sm-3 col-xs-12 item">   -Содержимое-   </div>
            <div class="col-md-3 col-sm-3 col-xs-12 item">   -Содержимое-   </div>
            ...
     </div>
     */
    protected string GetRowStart(object obj)
    {
      int index = Convert.ToInt32(obj);
      double indexMinus1 = index - 1;
      object result = indexMinus1 / col;

      // проверяем на целое число, если целое выводим <div class="row">
      if (Convert.ToInt32(result) == Convert.ToDouble(result))
        return "<div class=\"row\">";

      return "";
    }

    // Для вставки элемента <div class=\"row\">  в конец строки данных 
    // содержащее колонки размером col (смотрите перменную col)
    protected string GetRowEnd(object obj)
    {
      int index = Convert.ToInt32(obj);

      if ((index % col == 0) ||
        (GetRowCount() == index))
        return "</div>";

      return "";
    }

    int GetRowCount()
    {
      int count = 0;
      string xml = srcCategories.Data;

      XmlReader xmlReader = XmlReader.Create(
        new StringReader(xml));

      while (xmlReader.Read())
      {
        if (xmlReader.NodeType == XmlNodeType.Element &&
            xmlReader.Name.Equals("item"))
          count++;
      }

      return count;
    }

  }
}