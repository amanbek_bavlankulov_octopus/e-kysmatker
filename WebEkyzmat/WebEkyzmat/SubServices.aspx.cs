﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebEkyzmat
{
  public partial class SubServices : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      Content masterPage = Page.Master as Content;
      masterPage.MyProcName = "FindSubServices";
      masterPage.MyRouteName = "Service";


      Repeater1.DataSourceID = masterPage.MyDataSourceName;

      Repeater2.DataSourceID = masterPage.MyDataSourceName;

      if (masterPage.HasHistory)
      {
        masterPage.UnwindPage("SubServices.aspx");
        HyperLink1.NavigateUrl = masterPage.TopUrl;
      }
      else
        HyperLink1.Visible = false;

      masterPage.SaveUrl(GetReturnUrl());
    }

    string GetReturnUrl()
    {
      return "SubServices.aspx?id=" + Request.QueryString["id"];
    }

    public string GetUrl(object id)
    {
      return "Organizations.aspx?id=" + id;
    }

    public string GetDialogLink(object id)
    {
      return "#dialog_" + id;
    }

    public string BuildDialogId(object id)
    {
      return "dialog_" + id;
    }

    public string ParseGuide(object data)
    {
      string xml = data.ToString();
      XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(xml);

      XmlNamespaceManager namespaces = new XmlNamespaceManager(xmlDoc.NameTable);
      namespaces.AddNamespace("ns", "http://www.w3.org/2005/Atom");

      string guideHtml = "";

      foreach (XmlNode node in xmlDoc.SelectNodes("//ns:step", namespaces))
      {
        guideHtml += "<p>" + node.InnerText + "</p>";
        //node.se
      }


      return guideHtml;
    }

    public string GetStandartCount(object data)
    {
      string xml = data.ToString();
      XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(xml);

      XmlNamespaceManager namespaces = new XmlNamespaceManager(xmlDoc.NameTable);
      namespaces.AddNamespace("ns", "http://www.w3.org/2005/Atom");

      int count = 0;

      foreach (XmlNode node in xmlDoc.SelectNodes("//ns:step", namespaces))
      {
        count++;
      }


      return count.ToString();
    }
   

  }
}