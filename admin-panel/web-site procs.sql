CREATE PROC FindCategories 
	@id INT
AS
SELECT
*
FROM categories AS c
WHERE id = @id OR @id = 0
GO

CREATE PROC FindServices  
	@categoryId INT
AS
SELECT
	*
FROM services AS s
WHERE s.service_group_id = @categoryId
GO

CREATE PROC FindSubServices  
	@serviceId INT
AS
SELECT
	*
FROM subservices AS s
WHERE s.services_id = @serviceId
GO

CREATE PROC FindOrganizations 
	@subserviceId INT
AS
SELECT
	c.*
FROM Organizations AS c
INNER JOIN link_subservices_organizations AS lso ON lso.organization_id = c.id
WHERE
	lso.subservice_id = @subserviceId
GO

CREATE PROC FinOrganizationInfo
	@organizationId INT
AS
SELECT 
	* 
FROM 
	organizations AS o 
WHERE
	id = @organizationId
GO