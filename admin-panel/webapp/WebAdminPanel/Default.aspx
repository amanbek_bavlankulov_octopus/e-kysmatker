﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebAdminPanel._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-top: 50px"></div>
    <h3>Выберите данные для редактирования</h3>
    <div class="tiles">
        <div class="tile double selected bg-grey-cascade" id="#categories">
            <div class="corner">
            </div>
            <div class="check">
            </div>
            <div class="tile-body" onclick="javascript:window.location.href = 'Pages/Categories.aspx';">
                <%--<a href="Pages/Categories.aspx">
						</a>--%>
                <h3>Отрасли услуг</h3>
                <br />
                <p>
                    Нажмите для редактирования списка категорий и их изменения активности 
                </p>
            </div>
            <div class="tile-object">
                <div class="name">
                    <i class="fa fa-edit"></i>
                </div>
                <div class="number">
                </div>
            </div>
        </div>
        <div class="tile double selected bg-grey-cascade" id="#categories">
            <div class="corner">
            </div>
            <div class="check">
            </div>
            <div class="tile-body" onclick="javascript:window.location.href = 'Pages/Services.aspx';">
                <%--<a href="Pages/Categories.aspx">
						</a>--%>
                <h3>Направления услуг</h3>
                <br />
                <p>
                    Нажмите для редактирования направлений услуг и их изменения их принадлежности к категориям
                </p>
            </div>
            <div class="tile-object">
                <div class="name">
                    <i class="fa fa-edit"></i>
                </div>
                <div class="number">
                </div>
            </div>
        </div>
        <div class="tile double selected bg-grey-cascade" id="#categories">
            <div class="corner">
            </div>
            <div class="check">
            </div>
            <div class="tile-body" onclick="javascript:window.location.href = 'Pages/Subservices.aspx';">
                <%--<a href="Pages/Categories.aspx">
						</a>--%>
                <h3>Список услуг</h3>
                <br />
                <p>
                    Нажмите для редактирования списка услуг и их изменения их принадлежности к направлениям
                </p>
            </div>
            <div class="tile-object">
                <div class="name">
                    <i class="fa fa-edit"></i>
                </div>
                <div class="number">
                </div>
            </div>
        </div>
        <div class="tile double selected bg-grey-cascade" id="#categories">
            <div class="corner">
            </div>
            <div class="check">
            </div>
            <div class="tile-body" onclick="javascript:window.location.href = 'Pages/Organizations.aspx';">

                <h3>Список организаций</h3>
                <br />
                <p>
                    Нажмите для редактирования списка организаций и их добавления
                </p>
            </div>
            <div class="tile-object">
                <div class="name">
                    <i class="fa fa-edit"></i>
                </div>
                <div class="number">
                </div>
            </div>
        </div>
        <div class="tile double selected bg-grey-cascade" id="#categories">
            <div class="corner">
            </div>
            <div class="check">
            </div>
            <div class="tile-body" onclick="javascript:window.location.href = 'Account/Users.aspx';">
                <%--<a href="Pages/Categories.aspx">
						</a>--%>
                <h3>Пользователи системы</h3>
                <br />
                <p>
                    Нажмите для редактирования списка пользователей
                </p>
            </div>
            <div class="tile-object">
                <div class="name">
                    <i class="fa fa-users"></i>
                </div>
                <div class="number">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
