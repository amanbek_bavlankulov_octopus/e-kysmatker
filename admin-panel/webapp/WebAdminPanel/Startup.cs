﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebAdminPanel.Startup))]
namespace WebAdminPanel
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
