﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Organizations.aspx.cs" Inherits="WebAdminPanel.Pages.Organizations" ValidateRequest="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h3>Организации</h3>
        <div >
            Наименование :
        <asp:TextBox CssClass="form-control" ID="tbxName" runat="server" />
            Адрес :
        <asp:TextBox CssClass="form-control" ID="tbxAddress" runat="server" />
            Описание :
        <asp:TextBox CssClass="form-control" ID="tbxDesc" runat="server" />
            Номер :
        <asp:TextBox CssClass="form-control" ID="tbxTelno" runat="server" />
            Почта :
        <asp:TextBox CssClass="form-control" ID="tbxEmail" runat="server" />
            Веб-сайт :
        <asp:TextBox CssClass="form-control" ID="tbxWebsite" runat="server" />
            Фото :
        <asp:TextBox CssClass="form-control" ID="tbxPhoto" runat="server" />
            Широта :
        <asp:TextBox CssClass="form-control" ID="tbxLatitude" runat="server" />
            Долгота :
        <asp:TextBox CssClass="form-control" ID="tbxLongitude" runat="server" />
            Населенный пункт :
        <asp:DropDownList CssClass="form-control" ID="ddlCities" runat="server" DataSourceID="citiesList" DataTextField="name" DataValueField="id" />
            <asp:SqlDataSource ID="citiesList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [id], [name] FROM [cities]"></asp:SqlDataSource>
            <br />
            <asp:Button ID="btnCancel" runat="server" Text="Отмена" CssClass="btn btn-primary" OnClick="btnCancel_Click" OnClientClick="javascript:return confirm('Вы действительно хотите вернуться на предыдущую страницу?')"/>
            <asp:Button ID="btnAdd" runat="server" Text="Добавить" CssClass="btn btn-danger " OnClick="btnAdd_Click"  OnClientClick="javascript:return confirm('Вы действительно хотите сохранить введенные данные?')"/>
        </div>
        <br />
        <asp:GridView EditRowStyle-BackColor="Snow" runat="server" CssClass="table table-bordered table-hover table-striped table-condenced" ID="grvOrganizations" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="organiztionsDatasource" AllowSorting="True" OnRowCancelingEdit="grvOrganizations_RowCancelingEdit" OnRowUpdating="grvOrganizations_RowUpdating" AllowPaging="True" PageSize="30">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" DeleteText="Удалить" EditText="Изменить" CancelText="Отмена" UpdateText="Сохранить" />
                <asp:BoundField ItemStyle-CssClass="col-md-3" DataField="name" HeaderText="Наименование" SortExpression="tel_no">
                </asp:BoundField>
                <asp:BoundField ItemStyle-CssClass="col-md-2" DataField="email" HeaderText="Почта" SortExpression="email">
                </asp:BoundField>
                <asp:BoundField ItemStyle-CssClass="col-md-2" DataField="desc" HeaderText="Описание" SortExpression="desc">
                </asp:BoundField>
                <asp:BoundField HeaderStyle-Width="100px" ItemStyle-CssClass="col-md-2"  ItemStyle-Width="150px" DataField="web_site" HeaderText="Веб-сайт" SortExpression="web_site">
                </asp:BoundField>
                <asp:BoundField HeaderStyle-Width="100px" ItemStyle-CssClass="col-md-2"  ItemStyle-Width="150px" DataField="photo_name" HeaderText="Фото" SortExpression="photo_name">
                </asp:BoundField>
                <asp:TemplateField HeaderText="Город" ItemStyle-CssClass="col-md-2"  SortExpression="city_id">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="citiesList" DataTextField="name" DataValueField="id" SelectedValue='<%# Eval("city_id")%>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("city_name") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>

            <EditRowStyle BackColor="Snow"></EditRowStyle>
        </asp:GridView>
        <asp:SqlDataSource ID="organiztionsDatasource" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [organizations] WHERE [id] = @original_id" InsertCommand="INSERT INTO [organizations] ([name], [address], [latitude], [longitude], [desc], [tel_no], [email], [web_site], [parent_organization_id], [photo_name], [city_id]) VALUES (@name, @address, @latitude, @longitude, @desc, @tel_no, @email, @web_site, @parent_organization_id, @photo_name, @city_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT o.*,c.id city_id, c.name city_name FROM [organizations] o inner join cities c on c.id = o.city_id" UpdateCommand="UPDATE [organizations] SET [name] = @name, [address] = @address, [latitude] = @latitude, [longitude] = @longitude, [desc] = @desc, [tel_no] = @tel_no, [email] = @email, [web_site] = @web_site, [parent_organization_id] = @parent_organization_id, [photo_name] = @photo_name, [city_id] = @city_id WHERE [id] = @original_id AND [name] = @original_name AND (([address] = @original_address) OR ([address] IS NULL AND @original_address IS NULL)) AND (([latitude] = @original_latitude) OR ([latitude] IS NULL AND @original_latitude IS NULL)) AND (([longitude] = @original_longitude) OR ([longitude] IS NULL AND @original_longitude IS NULL)) AND (([desc] = @original_desc) OR ([desc] IS NULL AND @original_desc IS NULL)) AND (([tel_no] = @original_tel_no) OR ([tel_no] IS NULL AND @original_tel_no IS NULL)) AND (([email] = @original_email) OR ([email] IS NULL AND @original_email IS NULL)) AND (([web_site] = @original_web_site) OR ([web_site] IS NULL AND @original_web_site IS NULL)) AND (([parent_organization_id] = @original_parent_organization_id) OR ([parent_organization_id] IS NULL AND @original_parent_organization_id IS NULL)) AND (([photo_name] = @original_photo_name) OR ([photo_name] IS NULL AND @original_photo_name IS NULL)) AND (([city_id] = @original_city_id) OR ([city_id] IS NULL AND @original_city_id IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_name" Type="String" />
                <asp:Parameter Name="original_address" Type="String" />
                <asp:Parameter Name="original_latitude" Type="Double" />
                <asp:Parameter Name="original_longitude" Type="Double" />
                <asp:Parameter Name="original_desc" Type="String" />
                <asp:Parameter Name="original_tel_no" Type="String" />
                <asp:Parameter Name="original_email" Type="String" />
                <asp:Parameter Name="original_web_site" Type="String" />
                <asp:Parameter Name="original_parent_organization_id" Type="Int32" />
                <asp:Parameter Name="original_photo_name" Type="String" />
                <asp:Parameter Name="original_city_id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="address" Type="String" />
                <asp:Parameter Name="latitude" Type="Double" />
                <asp:Parameter Name="longitude" Type="Double" />
                <asp:Parameter Name="desc" Type="String" />
                <asp:Parameter Name="tel_no" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web_site" Type="String" />
                <asp:Parameter Name="parent_organization_id" Type="Int32" />
                <asp:Parameter Name="photo_name" Type="String" />
                <asp:Parameter Name="city_id" Type="Int32" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="name" Type="String" />
                <asp:Parameter Name="address" Type="String" />
                <asp:Parameter Name="latitude" Type="Double" />
                <asp:Parameter Name="longitude" Type="Double" />
                <asp:Parameter Name="desc" Type="String" />
                <asp:Parameter Name="tel_no" Type="String" />
                <asp:Parameter Name="email" Type="String" />
                <asp:Parameter Name="web_site" Type="String" />
                <asp:Parameter Name="parent_organization_id" Type="Int32" />
                <asp:Parameter Name="photo_name" Type="String" />
                <asp:Parameter Name="city_id" Type="Int32" />
                <asp:Parameter Name="original_id" Type="Int32" />
                <asp:Parameter Name="original_name" Type="String" />
                <asp:Parameter Name="original_address" Type="String" />
                <asp:Parameter Name="original_latitude" Type="Double" />
                <asp:Parameter Name="original_longitude" Type="Double" />
                <asp:Parameter Name="original_desc" Type="String" />
                <asp:Parameter Name="original_tel_no" Type="String" />
                <asp:Parameter Name="original_email" Type="String" />
                <asp:Parameter Name="original_web_site" Type="String" />
                <asp:Parameter Name="original_parent_organization_id" Type="Int32" />
                <asp:Parameter Name="original_photo_name" Type="String" />
                <asp:Parameter Name="original_city_id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
    </div>
</asp:Content>
