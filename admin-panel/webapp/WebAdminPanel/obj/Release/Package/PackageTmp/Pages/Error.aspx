﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="WebAdminPanel.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="color:darkred">
        <h3>Ошибка</h3>
        <h5>Произошла ошибка при выполнении программы. Пожалуйста обратитесь к разработчику</h5>
        <h5>Текст ошибки:</h5>
            <asp:Label runat="server" ID="lblErrorMessage" />
    </div>
</asp:Content>
