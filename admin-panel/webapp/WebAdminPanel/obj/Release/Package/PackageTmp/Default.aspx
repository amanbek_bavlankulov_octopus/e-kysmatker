﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebAdminPanel._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Выберите данные для редактирования</h3>
    <ul class="list-group">
        <li class="list-group-item"><a href="Pages/Categories.aspx">Отрасли услуг</a></li>
        <li class="list-group-item"><a href="Pages/Services.aspx">Направления услуг</a></li>
        <li class="list-group-item"><a href="Pages/Subservices.aspx">Услуги</a></li>
        <li class="list-group-item"><a href="Pages/Organizations.aspx">Организации</a></li>
    </ul>
</asp:Content>
