﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAdminPanel.Pages
{
    public partial class Services : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.IsAuthenticated) Response.Redirect("../Account/Login.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("..//Default.aspx");
        }
        
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var name = tbxName.Text;
            var description = tbxDescription.Text;
            var categoryId = ddlCategory.SelectedValue;

            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name)) return;

            try
            {
                var query = "INSERT INTO Services ([name],[desc], [service_group_id]) VALUES (@name, @desc, @category_id)";
                var connectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ToString();
                var connection = new SqlConnection(connectionString);
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar)).Value = name;
                command.Parameters.Add(new SqlParameter("@desc", SqlDbType.NVarChar)).Value = description;
                command.Parameters.Add(new SqlParameter("@category_id", SqlDbType.NVarChar)).Value = categoryId;

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                grvServices.DataBind();
            }
            catch (SqlException ex)
            {
                var message = ex.Message;
                Session.Add("errorMessage", message);

                Server.Transfer("Error.aspx");
            }            
        }

        protected void grvServices_RowEditing(object sender, GridViewEditEventArgs e)
        {
            
        }

        protected void grvServices_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var row = grvServices.Rows[e.RowIndex].Cells;

            var id = grvServices.DataKeys[e.RowIndex].Value;
            var name = ((TextBox)row[1].Controls[0]).Text;
            var desc = ((TextBox)row[2].Controls[0]).Text;
            var categoryId = ((DropDownList)row[3].Controls[1]).SelectedValue;

            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                grvServices.EditIndex = -1;
                e.Cancel = true;
            }

            e.NewValues.Add("service_group_id", categoryId);
        }
    }
}