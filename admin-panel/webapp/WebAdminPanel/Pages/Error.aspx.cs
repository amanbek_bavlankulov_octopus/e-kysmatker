﻿using System;
using System.Web.UI;

namespace WebAdminPanel
{
    public partial class Error : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.IsAuthenticated) Response.Redirect("../Account/Login.aspx");

            if (Session["error"] == null) return;

            var message = Session["error"].ToString();
            lblErrorMessage.Text = Server.HtmlDecode(message.ToString());
        }
    }
}