﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Organizations.aspx.cs" Inherits="WebAdminPanel.Pages.Organizations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hfId" runat="server" />
    <div style="padding-top:50px">
        <h3>Организации</h3>
        <table>
            <tr>
                <td>Наименование :<asp:TextBox CssClass="form-control" ID="tbxName" runat="server" /></td>
                <td>Адрес :<asp:TextBox CssClass="form-control" ID="tbxAddress" runat="server" /></td>
                <td>Номер :<asp:TextBox CssClass="form-control" ID="tbxTelno" runat="server" /></td>
            </tr>
            <tr>
                <td>Веб-сайт :<asp:TextBox CssClass="form-control" ID="tbxWebsite" runat="server" /></td>
                <td>Почта :<asp:TextBox CssClass="form-control" ID="tbxEmail" runat="server" /></td>
                <td>Фото :
                    <asp:FileUpload ID="fuFileUpload" runat="server" CssClass="form-control" ClientIDMode="Static"/>
                    <asp:CheckBox ID="chkKeepImage" runat="server" CssClass="checker" Enabled="false" Checked="false" Visible="false" ToolTip="Сохранить старое изображение" />
                </td>
            </tr>
            <tr>
                <td>Населенный пункт :<asp:DropDownList CssClass="form-control" ID="ddlCities" runat="server" DataSourceID="citiesList" DataTextField="name" DataValueField="id" />
                    <asp:SqlDataSource ID="citiesList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT 0 [id], '' [name] UNION ALL SELECT [id], [name] FROM [cities]"></asp:SqlDataSource>
                </td>
                <td>Широта :<asp:TextBox CssClass="form-control" ID="tbxLatitude" runat="server" /></td>
                <td>Долгота :<asp:TextBox CssClass="form-control" ID="tbxLongitude" runat="server" /></td>
            </tr>
            <tr>
                <td colspan="3">Описание :<asp:TextBox TextMode="MultiLine" Rows="10" CssClass="form-control" ID="tbxDesc" runat="server" /></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnCancel" runat="server" Text="Отмена" CssClass="btn btn-primary" OnClick="btnCancel_Click" OnClientClick="javascript:return confirm('Вы действительно хотите вернуться на предыдущую страницу?')" />
        <asp:Button ID="btnAdd" runat="server" Text="Сохранить" CssClass="btn btn-danger " OnClick="btnAdd_Click" OnClientClick="javascript:return confirm('Вы действительно хотите сохранить введенные данные?')" />
    </div>
    <br />
    <hr />
    <div class="row form-group">
        <div class="col-md-1"><span class="caption-subject font-green-sharp bold uppercase">Поиск : </span></div>
        <div class="col-md-4">
            <asp:TextBox AutoCompleteType="Search" aria-controls="grvOrganizations" ID="tbxSearchName" placeholder="Название" runat="server" CssClass="form-control" />
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlSubservice" placeholder="Услуга" runat="server" CssClass="form-control" DataSourceID="subservicesList" DataTextField="name" DataValueField="id" />
            <asp:SqlDataSource ID="subservicesList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select 0 id,'' name union all SELECT [id], [name] FROM [subservices]"></asp:SqlDataSource>
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlService" placeholder="Направление услуги" runat="server" CssClass="form-control" DataSourceID="servicesList" DataTextField="name" DataValueField="id" />
            <asp:SqlDataSource ID="servicesList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select 0 id,'' name
union all
SELECT [id], [name] FROM [services]"></asp:SqlDataSource>
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlCategory" placeholder="Категория" runat="server" CssClass="form-control" DataSourceID="categoriesList" DataTextField="name" DataValueField="id" />
            <asp:SqlDataSource ID="categoriesList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select 0 id ,'' name
union all
SELECT [id], [name] FROM [categories]"></asp:SqlDataSource>
        </div>
        <div class="col-md-1">
            <asp:Button ID="btnSearch" Text="Найти" runat="server" CssClass="btn" OnClick="btnSearch_Click" />
        </div>
    </div>
    <hr />
    <asp:GridView EditRowStyle-BackColor="Snow" runat="server" CssClass="table dataTable table-scrollable table-bordered table-hover table-striped table-condenced" ID="grvOrganizations" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="organiztionsDatasource" AllowSorting="True" OnRowCancelingEdit="grvOrganizations_RowCancelingEdit" OnRowEditing="grvOrganizations_RowEditing" PageSize="25" AllowPaging="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" DeleteText="Удалить" EditText="Изменить" CancelText="Отмена" UpdateText="Сохранить" />
            <asp:BoundField ItemStyle-CssClass="col-md-3" DataField="name" HeaderText="Наименование" SortExpression="tel_no" HtmlEncode="false" HtmlEncodeFormatString="true">
                <ItemStyle CssClass="col-md-3"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField ItemStyle-CssClass="col-md-2" DataField="email" HeaderText="Почта" SortExpression="email">
                <ItemStyle CssClass="col-md-2"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField ItemStyle-CssClass="col-md-2" DataField="tel_no" HeaderText="Телефон" SortExpression="tel_no">
                <ItemStyle CssClass="col-md-2"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField HeaderStyle-Width="100px" ItemStyle-CssClass="col-md-2" ItemStyle-Width="150px" DataField="web_site" HeaderText="Веб-сайт" SortExpression="web_site">
                <HeaderStyle Width="100px"></HeaderStyle>

                <ItemStyle CssClass="col-md-2" Width="150px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="Город" ItemStyle-CssClass="col-md-2" SortExpression="city_id">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="citiesList" DataTextField="name" DataValueField="id" SelectedValue='<%# Eval("cityId")%>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Literal ID="Literal1" runat="server" Text='<%# Bind("city_name") %>'></asp:Literal>
                </ItemTemplate>

                <ItemStyle CssClass="col-md-2"></ItemStyle>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="Snow"></EditRowStyle>
    </asp:GridView>
    <asp:SqlDataSource ID="organiztionsDatasource" runat="server" ConflictDetection="CompareAllValues"
        ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        DeleteCommand="DELETE FROM [organizations] WHERE [id] = @original_id"
        InsertCommand="INSERT INTO [organizations] ([name], [address], [latitude], [longitude], [desc], [tel_no], [email], [web_site], [parent_organization_id], [photo_name], [city_id]) VALUES (@name, @address, @latitude, @longitude, @desc, @tel_no, @email, @web_site, @parent_organization_id, @photo_name, @city_id)" OldValuesParameterFormatString="original_{0}"
        SelectCommand="SELECT o.*,ISNULL(c.id, 0) cityId, ISNULL(c.name, '') city_name FROM [organizations] o left join cities c on c.id = o.city_id" UpdateCommand="UPDATE [organizations] SET [name] = @name, [address] = @address, [latitude] = @latitude, [longitude] = @longitude, [desc] = @desc, [tel_no] = @tel_no, [email] = @email, [web_site] = @web_site, [parent_organization_id] = @parent_organization_id, [photo_name] = @photo_name, [city_id] = @city_id WHERE [id] = @original_id AND [name] = @original_name AND (([address] = @original_address) OR ([address] IS NULL AND @original_address IS NULL)) AND (([latitude] = @original_latitude) OR ([latitude] IS NULL AND @original_latitude IS NULL)) AND (([longitude] = @original_longitude) OR ([longitude] IS NULL AND @original_longitude IS NULL)) AND (([desc] = @original_desc) OR ([desc] IS NULL AND @original_desc IS NULL)) AND (([tel_no] = @original_tel_no) OR ([tel_no] IS NULL AND @original_tel_no IS NULL)) AND (([email] = @original_email) OR ([email] IS NULL AND @original_email IS NULL)) AND (([web_site] = @original_web_site) OR ([web_site] IS NULL AND @original_web_site IS NULL)) AND (([parent_organization_id] = @original_parent_organization_id) OR ([parent_organization_id] IS NULL AND @original_parent_organization_id IS NULL)) AND (([photo_name] = @original_photo_name) OR ([photo_name] IS NULL AND @original_photo_name IS NULL)) AND (([city_id] = @original_city_id) OR ([city_id] IS NULL AND @original_city_id IS NULL))">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_address" Type="String" />
            <asp:Parameter Name="original_latitude" Type="Double" />
            <asp:Parameter Name="original_longitude" Type="Double" />
            <asp:Parameter Name="original_desc" Type="String" />
            <asp:Parameter Name="original_tel_no" Type="String" />
            <asp:Parameter Name="original_email" Type="String" />
            <asp:Parameter Name="original_web_site" Type="String" />
            <asp:Parameter Name="original_parent_organization_id" Type="Int32" />
            <asp:Parameter Name="original_photo_name" Type="String" />
            <asp:Parameter Name="original_city_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="latitude" Type="Double" />
            <asp:Parameter Name="longitude" Type="Double" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="tel_no" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="web_site" Type="String" />
            <asp:Parameter Name="parent_organization_id" Type="Int32" />
            <asp:Parameter Name="photo_name" Type="String" />
            <asp:Parameter Name="city_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="address" Type="String" />
            <asp:Parameter Name="latitude" Type="Double" />
            <asp:Parameter Name="longitude" Type="Double" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="tel_no" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="web_site" Type="String" />
            <asp:Parameter Name="parent_organization_id" Type="Int32" />
            <asp:Parameter Name="photo_name" Type="String" />
            <asp:Parameter Name="city_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_address" Type="String" />
            <asp:Parameter Name="original_latitude" Type="Double" />
            <asp:Parameter Name="original_longitude" Type="Double" />
            <asp:Parameter Name="original_desc" Type="String" />
            <asp:Parameter Name="original_tel_no" Type="String" />
            <asp:Parameter Name="original_email" Type="String" />
            <asp:Parameter Name="original_web_site" Type="String" />
            <asp:Parameter Name="original_parent_organization_id" Type="Int32" />
            <asp:Parameter Name="original_photo_name" Type="String" />
            <asp:Parameter Name="original_city_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    </div>
</asp:Content>
