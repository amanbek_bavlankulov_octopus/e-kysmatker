﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAdminPanel.Pages
{
    public partial class Subservices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.IsAuthenticated) Response.Redirect("../Account/Login.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Default.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var name = tbxName.Text;
            var description = tbxDescription.Text;
            var categoryId = ddlCategory.SelectedValue;

            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name)) return;

            try
            {
                var query = "INSERT INTO Subservices ([name],[desc], [services_id]) VALUES (@name, @desc, @service_id)";
                var connectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ToString();
                var connection = new SqlConnection(connectionString);
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar)).Value = name;
                command.Parameters.Add(new SqlParameter("@desc", SqlDbType.NVarChar)).Value = description;
                command.Parameters.Add(new SqlParameter("@service_id", SqlDbType.NVarChar)).Value = categoryId;

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();

                grvSubservices.DataBind();
                
            }
            catch (SqlException ex)
            {
                var message = ex.Message;
                Session.Add("errorMessage", message);

                Server.Transfer("Error.aspx");
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedCategory = Convert.ToInt32(ddlCategory.SelectedValue);

            try
            {
                var query = "SELECT [id], [name] FROM Services s WHERE service_group_id = @category";
                var connectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ToString();
                var connection = new SqlConnection(connectionString);
                var command = new SqlCommand(query, connection);

                var adapter = new SqlDataAdapter(command);
                var dt = new DataTable();

                connection.Open();
                adapter.SelectCommand.Parameters.Add(new SqlParameter("@category", SqlDbType.Int)).Value = selectedCategory;
                adapter.Fill(dt);
                connection.Close();
                ddlService.DataSourceID = null;
                ddlService.DataSource = dt;
                ddlService.DataBind();
            }
            catch (SqlException ex)
            {
                var message = ex.Message;
                Session.Add("errorMessage", message);
                Server.Transfer("Error.aspx");
            }
        }

        protected void grvSubservices_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void ddlSubserviceCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedCategory = (sender as DropDownList).SelectedValue;
        }

        protected void grvSubservices_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var selectedSubservice = e.NewSelectedIndex;
            Response.Redirect("");
        }
    }
}