﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace WebAdminPanel.Pages
{
    public partial class Categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.IsAuthenticated) Response.Redirect("../Account/Login.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Default.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var name = tbxName.Text;
            var description = tbxDescription.Text;
            
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name)) return;

            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ToString();
                var connection = new SqlConnection(connectionString);
                var query = "INSERT INTO Categories ([name], [desc]) VALUES (@name, @desc)";
                var command = new SqlCommand(query, connection);

                command.Parameters.Add(new SqlParameter("@name", SqlDbType.NVarChar)).Value = name;
                command.Parameters.Add(new SqlParameter("@desc", SqlDbType.NVarChar)).Value = description;
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();

                grvCategories.DataBind();
            }
            catch (SqlException ex)
            {
                Session.Add("errorMessage", ex.Message);
                Server.Transfer("Error.aspx");
            }
        }
    }
}