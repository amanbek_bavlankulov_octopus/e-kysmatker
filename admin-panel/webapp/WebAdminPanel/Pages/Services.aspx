﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Services.aspx.cs" Inherits="WebAdminPanel.Pages.Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-top: 50px"></div>
    <h3>Направления услуг</h3>
    <p>
        Наименование
        <asp:TextBox CssClass="form-control" runat="server" ID="tbxName" />
        Описание
        <asp:TextBox CssClass="form-control" runat="server" ID="tbxDescription" />
        Отрасль
        <asp:DropDownList CssClass="form-control" runat="server" ID="ddlCategory" DataSourceID="categoriesList" DataTextField="name" DataValueField="id" />
        <asp:SqlDataSource ID="categoriesList" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [id], [name] FROM [categories]"></asp:SqlDataSource>
    </p>
    <p>
        <asp:Button CssClass="btn btn-primary" runat="server" ID="btnCancel" Text="Отмена" OnClientClick="javascript:comfirm('Вы уверены что хотите вернутсья на главную страницу?');" OnClick="btnCancel_Click" />
        <asp:Button CssClass="btn btn-danger" runat="server" ID="btnAdd" Text="Добавить" OnClientClick="javascript:comfirm('Вы уверены что хотите сохранить введенные данные?');" OnClick="btnAdd_Click" />
    </p>
    <asp:GridView CssClass="table table-striped table-hover table-condenced  table-bordered" runat="server" ID="grvServices" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="servicesSource" OnRowEditing="grvServices_RowEditing" OnRowUpdating="grvServices_RowUpdating">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" EditText="Изменить" UpdateText="Сохранить" CancelText="Отмена" DeleteText="Удалить" />
            <asp:BoundField ControlStyle-CssClass="form-control" DataField="name" HeaderText="Наименование" SortExpression="name" />
            <asp:BoundField ControlStyle-CssClass="form-control" DataField="desc" HeaderText="Описание" SortExpression="desc" />
            <asp:TemplateField HeaderText="Отрасль" SortExpression="category_name">
                <EditItemTemplate>
                    <asp:DropDownList CssClass="form-control" ID="ddlCategories" runat="server" DataSourceID="categoriesList" DataTextField="name" DataValueField="id">
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("category_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="servicesSource" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [services] WHERE [id] = @original_id" InsertCommand="INSERT INTO [services] ([name], [desc], [service_group_id]) VALUES (@name, @desc, @service_group_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT s.[id], s.[name], s.[desc],service_group_id, c.[name] category_name FROM [services] s inner join Categories c on c.id = s.service_group_id" UpdateCommand="UPDATE [services] SET [name] = @name, [desc] = @desc, [service_group_id] = @service_group_id WHERE [id] = @original_id">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_desc" Type="String" />
            <asp:Parameter Name="original_service_group_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="service_group_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="service_group_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_desc" Type="String" />
            <asp:Parameter Name="original_service_group_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
