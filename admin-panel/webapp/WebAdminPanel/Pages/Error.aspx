﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="WebAdminPanel.Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="color:darkred" class="page-title">
        <div style="padding-top:50px"></div>
        <h3>Ошибка</h3>
        <h4>Произошла ошибка при выполнении программы. Пожалуйста обратитесь к разработчику</h4>
        <h5>Текст ошибки:</h5>
            <asp:Label runat="server" ID="lblErrorMessage" />
    </div>
</asp:Content>
