﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Subservices.aspx.cs" Inherits="WebAdminPanel.Pages.Subservices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-top:50px"></div>
    <h3>Услуги</h3>
    <div style="padding:5%; width:50%">
        Наименование
        <asp:TextBox Width="100%" CssClass="form-control" runat="server" ID="tbxName" />
        Описание
        <asp:TextBox Width="100%" CssClass="form-control" runat="server" ID="tbxDescription" />
        Направление
        <asp:DropDownList Width="100%" CssClass="form-control" runat="server" ID="ddlService" DataSourceID="servicesDatasource" DataTextField="name" DataValueField="id" />
        <asp:SqlDataSource ID="servicesDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT id, name FROM services AS s ORDER BY name"></asp:SqlDataSource>
        Отрасль
        <asp:DropDownList Width="100%" CssClass="form-control" runat="server" ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="True" DataSourceID="categoriesDatasource" DataTextField="name" DataValueField="id" />
        <asp:SqlDataSource ID="categoriesDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [id], [name] FROM [categories]"></asp:SqlDataSource>
        <asp:Button CssClass="btn btn-primary" runat="server" ID="btnCancel" Text="Отмена" OnClientClick="javascript:confirm('Вы уверены что хотите возвратиться на главную страницу?');" OnClick="btnCancel_Click" />
        <asp:Button CssClass="btn btn-danger" runat="server" ID="btnAdd" Text="Добавить" OnClientClick="javascript:confirm('Вы уверены что хотите сохранить данные?');" OnClick="btnAdd_Click" />
    </div>
    <asp:GridView CssClass="table table-hover table-condenced table-striped table-bordered" runat="server" ID="grvSubservices" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="subservicesDatasource" OnRowEditing="grvSubservices_RowEditing" OnSelectedIndexChanging="grvSubservices_SelectedIndexChanging">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" EditText="Изменить" UpdateText="Сохранить" CancelText="Отмена" DeleteText="Удалить"/>
            <asp:BoundField ControlStyle-CssClass="form-control" DataField="name" HeaderText="Наименование" SortExpression="name" />
            <asp:BoundField ControlStyle-CssClass="form-control" DataField="desc" HeaderText="Описание" SortExpression="desc" />
            <asp:TemplateField HeaderText="Направление" SortExpression="service_name">
                <EditItemTemplate>
                    <asp:DropDownList CssClass="form-control" ID="ddlSubserviceServices" runat="server" DataSourceID="servicesDatasource" DataTextField="name" DataValueField="id" SelectedValue='<%# Bind("service_id") %>'>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("service_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Категория" SortExpression="category_name">
                <EditItemTemplate>
                    <asp:DropDownList CssClass="form-control"  ID="ddlSubserviceCategories" runat="server" AutoPostBack="True" DataSourceID="categoriesDatasource" DataTextField="name" DataValueField="id" OnSelectedIndexChanged="ddlSubserviceCategories_SelectedIndexChanged">
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("category_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="subservicesDatasource" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [subservices] WHERE [id] = @original_id" InsertCommand="INSERT INTO [subservices] ([name], [desc], [services_id]) VALUES (@name, @desc, @services_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT
                    su.id
                    , su.name
                    , su.[desc]
                    , s.id service_id
                    , s.name service_name
                    , c.name category_name
                    FROM [subservices] su
                    inner join [services] s on s.id = su.services_id
                    inner join [categories] c on c.id = s.service_group_id "
        UpdateCommand="UPDATE [subservices] SET [name] = @name, [desc] = @desc, [services_id] = @services_id WHERE [id] = @original_id AND [name] = @original_name AND (([desc] = @original_desc) OR ([desc] IS NULL AND @original_desc IS NULL)) AND [services_id] = @original_services_id">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_desc" Type="String" />
            <asp:Parameter Name="original_services_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="services_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="services_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_desc" Type="String" />
            <asp:Parameter Name="original_services_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
