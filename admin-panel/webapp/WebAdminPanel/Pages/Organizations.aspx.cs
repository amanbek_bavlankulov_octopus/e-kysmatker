﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAdminPanel.Pages
{
    public partial class Organizations : System.Web.UI.Page
    {
        bool _mode = false;
        static int orgId = 0;

        static string _photoPath = ConfigurationManager.AppSettings["organization_photo_path"];
        static string _photoUrl = ConfigurationManager.AppSettings["organization_photo_url"];

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.IsAuthenticated) Response.Redirect("../Account/Login.aspx");

            Page.Master.ValidateRequestMode = ValidateRequestMode.Disabled;
            _mode = Page.Request.QueryString.HasKeys();
            
            if (_mode && !Page.IsPostBack)
            {
                var queryValue = Page.Request.QueryString["id"].ToString();
                var organizationId = Convert.ToInt32(queryValue);
                orgId = organizationId;

                var query = "SELECT * FROM organizations WHERE id = @id";
                var command = GetSqlCommand(query);
                command.Parameters.AddWithValue("@id", organizationId);
                command.Connection.Open();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var name = reader["name"].ToString();
                    var desc = reader["desc"].ToString();
                    var address = reader["address"].ToString();
                    var website = reader["web_site"].ToString();
                    var email = reader["email"].ToString();
                    var phone = reader["tel_no"].ToString();
                    var latitude = reader["latitude"].ToString();
                    var longitude = reader["longitude"].ToString();
                    var cityId = reader["city_id"].ToString();
                    
                    tbxName.Text = name;
                    tbxDesc.Text = desc;
                    tbxAddress.Text = address;
                    tbxEmail.Text = email;
                    tbxWebsite.Text = website;
                    tbxTelno.Text = phone;
                    tbxLatitude.Text = latitude;
                    tbxLongitude.Text = longitude;                    
                }
                command.Connection.Close();

                if (!Page.IsPostBack) grvOrganizations.DataBind();
            }
        }

        protected void grvOrganizations_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Default.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var name = tbxName.Text;
            var address = tbxAddress.Text;
            var desc = tbxDesc.Text;
            var phone = tbxTelno.Text;
            var email = tbxEmail.Text;
            var website = tbxWebsite.Text;
            var photo = string.IsNullOrEmpty(fuFileUpload.FileName) ? string.Empty : _photoUrl + fuFileUpload.FileName;
            var latitude = Convert.ToDecimal(tbxLatitude.Text);
            var longitude = Convert.ToDecimal(tbxLongitude.Text);
            var city = ddlCities.SelectedValue;

            if(!string.IsNullOrEmpty(fuFileUpload.FileName))
                fuFileUpload.SaveAs(_photoPath + fuFileUpload.FileName);

            var query = _mode ?
                "UPDATE Organizations SET [name] = @name, [address] = @address, [latitude] = @lat, [longitude] = @lng, [desc] = @desc, [tel_no] = @phone, [email] = @email, [web_site] = @website, [photo_name] = " + (chkKeepImage.Checked ? "[photo_name]" : "@photo") + ", [city_id] = @city WHERE id = @id"
                : "INSERT INTO Organizations (name, address, latitude, longitude, [desc], tel_no, email, web_site, photo_name, city_id) VALUES (@name, @address, @lat, @lng, @desc, @phone, @email, @website, @photo, @city)";
            var command = GetSqlCommand(query);

            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@address", address);
            command.Parameters.AddWithValue("@lat", latitude);
            command.Parameters.AddWithValue("@lng", longitude);
            command.Parameters.AddWithValue("@desc", desc);
            command.Parameters.AddWithValue("@phone", phone);
            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@website", website);
            command.Parameters.AddWithValue("@photo", photo);
            if(city == "0")
                command.Parameters.AddWithValue("@city", DBNull.Value);
            else
                command.Parameters.AddWithValue("@city", city);

            if (_mode) command.Parameters.AddWithValue("@id", orgId);
            command.Connection.Open();
            command.ExecuteNonQuery();
            command.Connection.Close();

            grvOrganizations.DataBind();

            tbxSearchName.Text = name;
            btnSearch_Click(null, null);

            if (_mode) Response.Redirect("Organizations.aspx");
            
        }

        //protected void grvOrganizations_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    var selectedOrganization = Convert.ToInt32(grvOrganizations.DataKeys[e.RowIndex].Value);
        //    var cells = grvOrganizations.Rows[e.RowIndex].Cells;

        //    var name = (cells[1].Controls[0] as TextBox).Text;
        //    var email = (cells[2].Controls[0] as TextBox).Text;
        //    var telNo = (cells[3].Controls[0] as TextBox).Text;
        //    var desc = (cells[4].Controls[0] as TextBox).Text;
        //    var website = (cells[5].Controls[0] as TextBox).Text;
        //    var city = (cells[6].Controls[1] as DropDownList).SelectedValue;

        //    var query = "UPDATE Organizations SET [name] = @name, [tel_no] = @telNo, [email] = @email, [desc] = @desc, web_site = @website, city_id = @city WHERE id = @id";

        //    var command = GetSqlCommand(query);

        //    command.Parameters.AddWithValue("@name", name);
        //    command.Parameters.AddWithValue("@email", email);
        //    command.Parameters.AddWithValue("@desc", desc);
        //    command.Parameters.AddWithValue("@website", website);
        //    command.Parameters.AddWithValue("@telNo", telNo);
        //    command.Parameters.AddWithValue("@id", selectedOrganization);

        //    if (Convert.ToInt32(city) == 0)
        //        command.Parameters.AddWithValue("@city", DBNull.Value);
        //    else
        //        command.Parameters.AddWithValue("@city", city);

        //    command.Connection.Open();
        //    command.ExecuteNonQuery();
        //    command.Connection.Close();

        //    grvOrganizations.DataBind();
        //}

        private SqlCommand GetSqlCommand(string query)
        {
            var connection = GetSqlConnection();
            var command = new SqlCommand(query, connection);

            return command;
        }

        private SqlConnection GetSqlConnection()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["defaultConnection"].ToString();
            var connection = new SqlConnection(connectionString);

            return connection;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var selectedName = tbxSearchName.Text;
            var selectedCategory = ddlCategory.SelectedValue;
            var selectedService = ddlService.SelectedValue;
            var selectedSubservice = ddlSubservice.SelectedValue;

            var query = @"SELECT o.*, ct.name city_name
                            FROM organizations AS o
                                LEFT OUTER JOIN cities ct on ct.id = o.city_id
                                FULL OUTER JOIN link_subservices_organizations AS lso ON lso.organization_id = o.id
                                FULL OUTER JOIN subservices AS s ON s.id = lso.subservice_id
                                FULL OUTER JOIN services AS ss ON ss.id = s.services_id
                                FULL OUTER JOIN categories AS c ON c.id = ss.service_group_id                                
                            WHERE   (@category_id = 0 OR (@category_id <> 0 AND c.id = @category_id))
                                    AND (@service_id = 0 OR (@service_id <> 0 AND ss.id = @service_id))
                                    AND (@subservice_id = 0 OR (@subservice_id <> 0 AND s.id = @subservice_id))
                                    AND (@name = '' OR (@name <> '' AND o.[name] LIKE '%' + @name + '%'))";

            var command = GetSqlCommand(query);

            command.Parameters.AddWithValue("@name",selectedName);
            command.Parameters.AddWithValue("@subservice_id", selectedSubservice);
            command.Parameters.AddWithValue("@service_id", selectedService);
            command.Parameters.AddWithValue("@category_id", selectedCategory);

            var dt = new DataTable();

            var adapter = new SqlDataAdapter(command);
            command.Connection.Open();
            adapter.Fill(dt);
            command.Connection.Close();

            grvOrganizations.DataSourceID = null;
            grvOrganizations.DataSource = dt;
            grvOrganizations.DataBind();
        }

        protected void grvOrganizations_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grvOrganizations.EditIndex = -1;
            var organizationId = grvOrganizations.DataKeys[e.NewEditIndex].Value;
            Response.Redirect("Organizations.aspx?id="+organizationId.ToString());
        }

        protected void grvOrganizations_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var cell = e.Row.Cells[3].Text;
                var text = HttpUtility.HtmlDecode(cell);
                cell = text;
            }
        }
    }
}