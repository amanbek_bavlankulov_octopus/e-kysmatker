﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="WebAdminPanel.Pages.Categories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-top:50px"></div>
    <h3>Отрасли услуг</h3>
    <div>
        Наименование
        <asp:TextBox CssClass="form-control" runat="server" ID="tbxName" />
        Описание
        <asp:TextBox CssClass="form-control" runat="server" ID="tbxDescription" />
        <br />
        <asp:Button CssClass="btn btn-primary" runat="server" ID="btnCancel" Text="Отмена" OnClick="btnCancel_Click" OnClientClick="javascript:return confirm('Вы уверены что хотите вернуться на главную страницу?');" />
        <asp:Button CssClass="btn btn-danger" runat="server" ID="btnAdd" Text="Добавить" OnClick="btnAdd_Click" OnClientClick="javascript:return confirm('Вы уверены что хотите сохранить введенные данные?');" />
    </div>
    <br />
    <asp:GridView CssClass="table table-condenced table-striped table-hover  table-bordered" runat="server" ID="grvCategories" DataSourceID="categoriesSource" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" AllowPaging="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" EditText="Изменить" UpdateText="Сохранить" CancelText="Отмена" DeleteText="Удалить"/>
            <asp:BoundField ControlStyle-CssClass="form-control"  DataField="name" HeaderText="Название" SortExpression="name" />
            <asp:BoundField ControlStyle-CssClass="form-control" DataField="desc" HeaderText="Описание" SortExpression="desc" />
            <asp:CheckBoxField DataField="active" HeaderText="Активен" ReadOnly="false" SortExpression="active" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="categoriesSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [categories] WHERE [id] = @id" InsertCommand="INSERT INTO [categories] ([name], [desc]) VALUES (@name, @desc)" SelectCommand="SELECT [id], [name], [desc], active FROM [categories]" UpdateCommand="UPDATE [categories] SET [name] = @name, [desc] = @desc, [active]=@active WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="desc" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="desc" Type="String" />
            <asp:Parameter Name="active" Type="Int32" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

