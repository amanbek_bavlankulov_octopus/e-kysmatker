﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAdminPanel.Pages
{
    public partial class EditUserCategory : System.Web.UI.Page
    {
        static string _userId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Page.Request.QueryString["id"] == null) Response.Redirect("Users.aspx");

                _userId = Page.Request.QueryString["id"];

                BindGridView();
            }
        }

        private void BindGridView()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            var connection = new SqlConnection(connectionString);
            var query = @"SELECT luc.category_id, c.name category_name, luc.allowed FROM LinkUserCategories AS luc
                            INNER JOIN AspNetUsers AS anu ON anu.Id = luc.userid
                            INNER JOIN categories AS c ON c.id = luc.category_id
                            WHERE anu.id = @id";

            var command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", _userId);

            connection.Open();
            var adapter = new SqlDataAdapter(command);
            var dt = new DataTable();
            adapter.Fill(dt);
            grvCategories.DataSource = dt;
            grvCategories.DataBind();
            connection.Close();
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            var checkBox = (CheckBox)sender;
            var categoryId = checkBox.ToolTip;
            var allowed = checkBox.Checked;

            var query = @"UPDATE LinkUserCategories
                            SET
                        allowed = @allowed
                        WHERE
                        category_id = @id";
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            var command = new SqlCommand(query, connection);

            connection.Open();
            command.Parameters.AddWithValue("@allowed", allowed);
            command.Parameters.AddWithValue("@id", categoryId);
            command.ExecuteNonQuery();
            connection.Close();

            BindGridView();
        }
    }
}