﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAdminPanel.Account
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.User.Identity.IsAuthenticated) Response.Redirect("Login.aspx");
            //var isAdmin = Page.User.Identity.Name == "ekyzmat@gmail.com";
            var isAdmin = Page.User.Identity.Name == "amanbek.bavlankulov@gmail.com";

            if (!isAdmin) Response.Redirect("../Pages/Default.aspx");
        }

        protected void grvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            var userId = grvUsers.DataKeys[e.NewEditIndex].Value;
            Response.Redirect("EditUserCategory.aspx?id=" + userId);
        }
    }
}