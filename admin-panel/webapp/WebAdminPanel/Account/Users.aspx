﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="WebAdminPanel.Account.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-top: 50px"></div>
    <br />
            <div class="page-toolbar">
                <a class="btn btn-default" href="javascript:window.location.href='Register.aspx'"><i class="fa fa-user"></i>&nbsp;Новый пользователь</a>
            </div>
            <h3>Пользователи системы</h3>
            <asp:GridView ID="grvUsers" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="False" DataSourceID="userDatasource" AllowSorting="True" DataKeyNames="Id" OnRowEditing="grvUsers_RowEditing">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" DeleteText="Удалить" ShowEditButton="true" EditText="Разрешения"/>
                    <asp:BoundField DataField="Email" HeaderText="Почта" SortExpression="Email" />
                    <asp:BoundField DataField="UserName" HeaderText="Пользователь" SortExpression="UserName" />                    
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="userDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                SelectCommand="SELECT * FROM [AspNetUsers] WHERE ([UserName] NOT LIKE '%' + @UserName + '%')" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [AspNetUsers] WHERE [Id] = @original_Id AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND [EmailConfirmed] = @original_EmailConfirmed AND (([PasswordHash] = @original_PasswordHash) OR ([PasswordHash] IS NULL AND @original_PasswordHash IS NULL)) AND (([SecurityStamp] = @original_SecurityStamp) OR ([SecurityStamp] IS NULL AND @original_SecurityStamp IS NULL)) AND (([PhoneNumber] = @original_PhoneNumber) OR ([PhoneNumber] IS NULL AND @original_PhoneNumber IS NULL)) AND [PhoneNumberConfirmed] = @original_PhoneNumberConfirmed AND [TwoFactorEnabled] = @original_TwoFactorEnabled AND (([LockoutEndDateUtc] = @original_LockoutEndDateUtc) OR ([LockoutEndDateUtc] IS NULL AND @original_LockoutEndDateUtc IS NULL)) AND [LockoutEnabled] = @original_LockoutEnabled AND [AccessFailedCount] = @original_AccessFailedCount AND [UserName] = @original_UserName AND (([Discriminator] = @original_Discriminator) OR ([Discriminator] IS NULL AND @original_Discriminator IS NULL))" InsertCommand="INSERT INTO [AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [Discriminator]) VALUES (@Id, @Email, @EmailConfirmed, @PasswordHash, @SecurityStamp, @PhoneNumber, @PhoneNumberConfirmed, @TwoFactorEnabled, @LockoutEndDateUtc, @LockoutEnabled, @AccessFailedCount, @UserName, @Discriminator)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [AspNetUsers] SET [Email] = @Email, [EmailConfirmed] = @EmailConfirmed, [PasswordHash] = @PasswordHash, [SecurityStamp] = @SecurityStamp, [PhoneNumber] = @PhoneNumber, [PhoneNumberConfirmed] = @PhoneNumberConfirmed, [TwoFactorEnabled] = @TwoFactorEnabled, [LockoutEndDateUtc] = @LockoutEndDateUtc, [LockoutEnabled] = @LockoutEnabled, [AccessFailedCount] = @AccessFailedCount, [UserName] = @UserName, [Discriminator] = @Discriminator WHERE [Id] = @original_Id AND (([Email] = @original_Email) OR ([Email] IS NULL AND @original_Email IS NULL)) AND [EmailConfirmed] = @original_EmailConfirmed AND (([PasswordHash] = @original_PasswordHash) OR ([PasswordHash] IS NULL AND @original_PasswordHash IS NULL)) AND (([SecurityStamp] = @original_SecurityStamp) OR ([SecurityStamp] IS NULL AND @original_SecurityStamp IS NULL)) AND (([PhoneNumber] = @original_PhoneNumber) OR ([PhoneNumber] IS NULL AND @original_PhoneNumber IS NULL)) AND [PhoneNumberConfirmed] = @original_PhoneNumberConfirmed AND [TwoFactorEnabled] = @original_TwoFactorEnabled AND (([LockoutEndDateUtc] = @original_LockoutEndDateUtc) OR ([LockoutEndDateUtc] IS NULL AND @original_LockoutEndDateUtc IS NULL)) AND [LockoutEnabled] = @original_LockoutEnabled AND [AccessFailedCount] = @original_AccessFailedCount AND [UserName] = @original_UserName AND (([Discriminator] = @original_Discriminator) OR ([Discriminator] IS NULL AND @original_Discriminator IS NULL))">
                
                <DeleteParameters>
                    <asp:Parameter Name="original_Id" Type="String" />
                    <asp:Parameter Name="original_Email" Type="String" />
                    <asp:Parameter Name="original_EmailConfirmed" Type="Boolean" />
                    <asp:Parameter Name="original_PasswordHash" Type="String" />
                    <asp:Parameter Name="original_SecurityStamp" Type="String" />
                    <asp:Parameter Name="original_PhoneNumber" Type="String" />
                    <asp:Parameter Name="original_PhoneNumberConfirmed" Type="Boolean" />
                    <asp:Parameter Name="original_TwoFactorEnabled" Type="Boolean" />
                    <asp:Parameter Name="original_LockoutEndDateUtc" Type="DateTime" />
                    <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                    <asp:Parameter Name="original_AccessFailedCount" Type="Int32" />
                    <asp:Parameter Name="original_UserName" Type="String" />
                    <asp:Parameter Name="original_Discriminator" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Id" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                    <asp:Parameter Name="PasswordHash" Type="String" />
                    <asp:Parameter Name="SecurityStamp" Type="String" />
                    <asp:Parameter Name="PhoneNumber" Type="String" />
                    <asp:Parameter Name="PhoneNumberConfirmed" Type="Boolean" />
                    <asp:Parameter Name="TwoFactorEnabled" Type="Boolean" />
                    <asp:Parameter Name="LockoutEndDateUtc" Type="DateTime" />
                    <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                    <asp:Parameter Name="AccessFailedCount" Type="Int32" />
                    <asp:Parameter Name="UserName" Type="String" />
                    <asp:Parameter Name="Discriminator" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:Parameter DefaultValue="amanbek.bavlankulov@gmail.com" Name="UserName" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="EmailConfirmed" Type="Boolean" />
                    <asp:Parameter Name="PasswordHash" Type="String" />
                    <asp:Parameter Name="SecurityStamp" Type="String" />
                    <asp:Parameter Name="PhoneNumber" Type="String" />
                    <asp:Parameter Name="PhoneNumberConfirmed" Type="Boolean" />
                    <asp:Parameter Name="TwoFactorEnabled" Type="Boolean" />
                    <asp:Parameter Name="LockoutEndDateUtc" Type="DateTime" />
                    <asp:Parameter Name="LockoutEnabled" Type="Boolean" />
                    <asp:Parameter Name="AccessFailedCount" Type="Int32" />
                    <asp:Parameter Name="UserName" Type="String" />
                    <asp:Parameter Name="Discriminator" Type="String" />
                    <asp:Parameter Name="original_Id" Type="String" />
                    <asp:Parameter Name="original_Email" Type="String" />
                    <asp:Parameter Name="original_EmailConfirmed" Type="Boolean" />
                    <asp:Parameter Name="original_PasswordHash" Type="String" />
                    <asp:Parameter Name="original_SecurityStamp" Type="String" />
                    <asp:Parameter Name="original_PhoneNumber" Type="String" />
                    <asp:Parameter Name="original_PhoneNumberConfirmed" Type="Boolean" />
                    <asp:Parameter Name="original_TwoFactorEnabled" Type="Boolean" />
                    <asp:Parameter Name="original_LockoutEndDateUtc" Type="DateTime" />
                    <asp:Parameter Name="original_LockoutEnabled" Type="Boolean" />
                    <asp:Parameter Name="original_AccessFailedCount" Type="Int32" />
                    <asp:Parameter Name="original_UserName" Type="String" />
                    <asp:Parameter Name="original_Discriminator" Type="String" />
                </UpdateParameters>
                
            </asp:SqlDataSource>

</asp:Content>
