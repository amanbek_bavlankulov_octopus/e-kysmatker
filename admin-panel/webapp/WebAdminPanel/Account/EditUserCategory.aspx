﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditUserCategory.aspx.cs" Inherits="WebAdminPanel.Pages.EditUserCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-top:50px"></div>
    <h3>Категории пользователя <asp:Label ID="lblUser" runat="server" /></h3>
    <asp:GridView ID="grvCategories" runat="server"  DataKeyNames="category_id" CssClass="table-bordered table-hover table-condensed" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField ="category_name" HeaderText="Категории" />
            <asp:TemplateField HeaderText="Разрешено редактирование" ItemStyle-Width="100px" ItemStyle-CssClass="align:center">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("allowed") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HiddenField ID="hdnOldAllowed" runat="server" Value='<%# Eval("allowed") %>' />
                    <asp:HiddenField ID="hdnCategoryId" runat="server" Value='<%# Eval("category_id") %>' />
                    <asp:CheckBox ID="CheckBox1" runat="server" tooltip='<%# Eval("category_id") %>' Checked='<%# Bind("allowed") %>' Enabled='true' OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
