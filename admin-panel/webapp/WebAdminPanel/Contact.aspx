﻿<%@ Page Title="Контакты" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebAdminPanel.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Техническая поддержка.</h3>
    <address>
        <strong>E-mail:</strong>   <a href="mailto:amanbek.bavlankulov@gmail.com">Amanbek Bavlankulov</a><br />
        <strong>Skype:</strong> <a href="skype:amanbek.bavlankulov">Amanbek Bavlankulov</a>
    </address>
</asp:Content>
