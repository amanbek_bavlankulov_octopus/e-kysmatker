﻿using AustinHarris.JsonRpc;
using MobileWebService.Objects;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MobileWebService
{
  /*
   * using GET Method
   * ----------------
   * http://<anyserver>/JsonRpc2Service.rpc?jsonrpc={"method": "helloWorld", "params": ["Hello World"], "id": 1  }
   * 
   * using POST Method
   * -----------------
   * Headers:
   * POST http://localhost:49718/json.rpc HTTP/1.1
   * User-Agent: Fiddler
   * Content-Type: Application/Json-Rpc
   * Host: localhost:49718
   * Content-Length: 62
   * 
   * Body:
   * {"method": "helloWorld", "params": ["Hello World"], "id": 1  }
   */

  public class JsonRpc2Service : JsonRpcService
  {
    //input {"method": "helloWorld", "params": ["Hello World"], "id": 1  }
    // output {"result":"Hello World Hello World","Error":null,"Id":"1"}
    [JsonRpcMethod("helloWorld")]
    public string helloWorld(string message)
    {
      return "Hello World " + message;
    }

    //input {"method": "GetServiceGroups", "params": [], "id": 1  }
    //output {"jsonrpc":"2.0","result":[{"Id":1,"Name":"Service group 1","Description":"Service group description 1"}],"id":1}
    [JsonRpcMethod]
    private List<ServiceGroup> GetServiceGroups()
    {
      List<ServiceGroup> serviceGroups = new List<ServiceGroup>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetServiceGroups", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
          serviceGroups.Add(new ServiceGroup()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
            Description = reader.IsDBNull(2) ? null : reader.GetString(2),
            Icon = reader.IsDBNull(3) ? null : reader.GetString(3),
            Tag = reader.IsDBNull(4) ? null : reader.GetString(4),
            DisplayFeedback = reader.GetBoolean(5),
          });
        }
      }

      return serviceGroups;
    }

    // input {"method": "GetAllServices", "params": [], "id": 1  }
    // output {"jsonrpc":"2.0","result":[{"Id":1,"Name":"Service 1","Description":"Service description 1","Service_Group_Id":1},{"Id":2,"Name":"Service 2","Description":"Service description 2","Service_Group_Id":2}],"id":1}
    [JsonRpcMethod]
    private List<Service> GetAllServices()
    {
      List<Service> services = new List<Service>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllServices", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
          services.Add(new Service()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
            Description = reader.IsDBNull(2) ? null : reader.GetString(2),
            Service_Group_Id = reader.IsDBNull(4) ? null : (Nullable<int>)reader.GetInt32(4),
            Tag = reader.IsDBNull(5) ? null : reader.GetString(5)
          });
        }
      }

      return services;
    }

    // input {"method": "GetAllSubservices", "params": [], "id": 1  }
    // output {"jsonrpc":"2.0","result":[{"Id":1,"Name":"Subservice 1","Description":"Subservice description 1","Service_Id":1},{"Id":2,"Name":"Subservice 2","Description":"Subservice description 2","Service_Id":2}],"id":1}
    [JsonRpcMethod]
    private List<Subservice> GetAllSubservices()
    {
      List<Subservice> subservices = new List<Subservice>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllSubservices", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          subservices.Add(new Subservice()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
            Description = reader.IsDBNull(2) ? null : reader.GetString(2),
            Guide = reader.IsDBNull(3) ? null : reader.GetString(3),
            Service_Id = reader.IsDBNull(4) ? null : (Nullable<int>)reader.GetInt32(4),
            Tag = reader.IsDBNull(5) ? null : reader.GetString(5)
          });
        }
      }

      return subservices;
    }

    // input {"method": "GetAllDepartments", "params": [], "id": 1  }
    // output {"jsonrpc":"2.0","result":[{"Id":1,"Name":"Subservice 1","Address":"Address 1","Description":"Subservice description 1","Phones":null,"Longitude":42.875705,"Latitude":74.611652,"Parent_Department_Id":1},{"Id":2,"Name":"Subservice 2","Address":"Address 2","Description":"Subservice description 2","Phones":null,"Longitude":42.490232,"Latitude":78.391496,"Parent_Department_Id":1}],"id":1}
    [JsonRpcMethod]
    private List<Department> GetAllDepartments()
    {
      List<Department> departments = new List<Department>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllDepartments", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          departments.Add(new Department()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
            Address = reader.IsDBNull(2) ? null : reader.GetString(2),
            Latitude = reader.IsDBNull(3) ? null : (Nullable<float>)reader.GetFloat(3),
            Longitude = reader.IsDBNull(4) ? null : (Nullable<float>)reader.GetFloat(4),
            Description = reader.IsDBNull(5) ? null : reader.GetString(5),
            Phones = reader.IsDBNull(6) ? null : reader.GetString(6),
            City = reader.IsDBNull(7) ? null : (Nullable<int>)reader.GetInt32(7),
            Email = reader.IsDBNull(8) ? null : reader.GetString(8),
            Website = reader.IsDBNull(9) ? null : reader.GetString(9),
            Parent_Department_Id = reader.IsDBNull(10) ? null : (Nullable<int>)reader.GetInt32(10),
            PhotoName = reader.IsDBNull(11) ? null : reader.GetString(11),
            Tag = reader.IsDBNull(12) ? null : reader.GetString(12),
            DownloadLink = reader.IsDBNull(13) ? null : reader.GetString(13)
          });
        }
      }

      return departments;
    }

    // input {"method": "GetAllSubservicesDepartments", "params": [], "id": 1  }
    // output {"jsonrpc":"2.0","result":[{"Id":1,"Service_Id":1,"Department_Id":1,"Steps":"","Details":"фывафыва","Price":1.0}],"id":1}
    [JsonRpcMethod]
    private List<SubserviceJoinDepartment> GetAllSubservicesDepartments()
    {
      List<SubserviceJoinDepartment> subserviceJoinDepartments = new List<SubserviceJoinDepartment>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllSubservicesDepartments", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();
        while (reader.Read())
        {
          subserviceJoinDepartments.Add(new SubserviceJoinDepartment()
          {
            Id = reader.GetInt32(0),
            Service_Id = reader.IsDBNull(1) ? null : (Nullable<int>)reader.GetInt32(1),
            Department_Id = reader.IsDBNull(2) ? null : (Nullable<int>)reader.GetInt32(2),
            //Steps = reader.IsDBNull(3) ? null : reader.GetString(3),
            Details = reader.IsDBNull(3) ? null : reader.GetString(3),
            Price = reader.IsDBNull(4) ? null : (Nullable<decimal>)reader.GetDecimal(4)
          });
        }
      }

      return subserviceJoinDepartments;
    }


    [JsonRpcMethod]
    private List<City> GetAllCities()
    {
      List<City> cities = new List<City>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllCities", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          cities.Add(new City()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
            District_Id = reader.IsDBNull(2) ? null : (Nullable<int>)reader.GetInt32(2),
          });
        }
      }

      return cities;
    }


    [JsonRpcMethod]
    private List<District> GetAllDistricts()
    {
      List<District> districts = new List<District>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllDistricts", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          districts.Add(new District()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
            Region_Id = reader.IsDBNull(2) ? null : (Nullable<int>)reader.GetInt32(2),
          });
        }
      }

      return districts;
    }



    [JsonRpcMethod]
    private List<Region> GetAllRegions()
    {
      List<Region> regions = new List<Region>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetAllRegions", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          regions.Add(new Region()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1),
          });
        }
      }

      return regions;
    }

    [JsonRpcMethod]
    private int? GetVersionInfo()
    {
      int? version = null;

      using (SqlCommand command = new SqlCommand("dbo.mws_GetVersionInfo", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          version = reader.IsDBNull(0) ? null : (Nullable<int>)reader.GetInt32(0);
        }
      }
      return version;
    }


    [JsonRpcMethod]
    private List<FeedbackAnswer> GetFeedbackAnswers()
    {
      List<FeedbackAnswer> feedbackAnswers = new List<FeedbackAnswer>();

      using (SqlCommand command = new SqlCommand("dbo.mws_GetFeedbackAnswers", ConnectionManager.GetSqlConnection()))
      {
        SqlDataReader reader = command.ExecuteReader();

        while (reader.Read())
        {
          feedbackAnswers.Add(new FeedbackAnswer()
          {
            Id = reader.GetInt32(0),
            Name = reader.IsDBNull(1) ? null : reader.GetString(1)
          });
        }
      }

      return feedbackAnswers;
    }


    [JsonRpcMethod]
    private void SetFeedback(int feedbackId, int subserviceOrganizationId, string name, string phone, string fully, string timely, string kindly, string discrimination, string sanitarly, string comment)
    {
      using (SqlCommand command = new SqlCommand("dbo.mws_SetFeedback", ConnectionManager.GetSqlConnection()))
      {
        command.CommandType = System.Data.CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@FeedbackId", feedbackId);
        command.Parameters.AddWithValue("@SubServiceOrganizationId", subserviceOrganizationId);
        command.Parameters.AddWithValue("@Name", name);
        command.Parameters.AddWithValue("@Phone", phone);
        command.Parameters.AddWithValue("@Fully", (string.IsNullOrEmpty(fully) || fully == "null") ? -1 : Convert.ToInt32(fully));
        command.Parameters.AddWithValue("@Timely", (string.IsNullOrEmpty(timely) || timely == "null") ? -1 : Convert.ToInt32(timely));
        command.Parameters.AddWithValue("@Kindly", (string.IsNullOrEmpty(kindly) || kindly == "null") ? -1 : Convert.ToInt32(kindly));
        command.Parameters.AddWithValue("@Discrimination", (string.IsNullOrEmpty(discrimination) || discrimination == "null") ? -1 : Convert.ToInt32(discrimination));
        command.Parameters.AddWithValue("@Sanitarly", (string.IsNullOrEmpty(sanitarly) || sanitarly == "null") ? -1 : Convert.ToInt32(sanitarly));
        command.Parameters.AddWithValue("@Comment", comment);
        command.ExecuteNonQuery();
      }
    }
  }
}