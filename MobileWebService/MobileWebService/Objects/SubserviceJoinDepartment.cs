﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWebService.Objects
{
  public class SubserviceJoinDepartment
  {
    public int Id { get; set; }
    public int? Service_Id { get; set; }
    public int? Department_Id { get; set; }
    //public string Steps { get; set; }
    public string Details { get; set; }
    public decimal? Price { get; set; }
  }
}