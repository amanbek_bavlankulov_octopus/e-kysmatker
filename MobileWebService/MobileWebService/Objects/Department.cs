﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWebService.Objects
{
  public class Department
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public float? Latitude  { get; set; }    
    public float? Longitude  { get; set; }    
    public string Description { get; set; }
    public string Phones { get; set; }
    public int? City { get; set; }
    public string Email { get; set; }
    public string Website { get; set; }
    public int? Parent_Department_Id { get; set; }
    public string PhotoName { get; set; }
    public string Tag { get; set; }
    public string DownloadLink { get; set; }
  }
}