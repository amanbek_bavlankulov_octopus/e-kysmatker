﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWebService.Objects
{
  public class Region
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}