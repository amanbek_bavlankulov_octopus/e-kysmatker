﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileWebService.Objects
{
  public class Subservice
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string Guide { get; set; }
    public int? Service_Id { get; set; }
    public string Tag { get; set; }
  }
}