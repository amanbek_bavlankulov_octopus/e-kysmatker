﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MobileWebService
{
  public static class ConnectionManager
  {
    public static SqlConnection GetSqlConnection()
    {
      string connStr = ConfigurationManager.AppSettings["ConnectionString"];

      SqlConnection conn = new SqlConnection(connStr);
      conn.Open();
      return conn;
    }

  }
}