/****** Object:  StoredProcedure [dbo].[mws_GetAllSubservices]    Script Date: 09/09/2015 23:37:10 ******/
DROP PROCEDURE [dbo].[mws_GetAllSubservices]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ruslan Abdraev
-- Create date: 09/09/2015
-- Description:	Возвращает список всех подуслуг(пломбирование, отбеливание зубов и т.д.)
-- =============================================
CREATE PROCEDURE [dbo].[mws_GetAllSubservices]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
		  ,[name]
		  ,[description]
		  ,[path_to_icon]
		  ,[service_id]
	  FROM [dbo].[SubService]

END
GO
