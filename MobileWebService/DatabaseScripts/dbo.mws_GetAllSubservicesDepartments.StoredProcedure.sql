/****** Object:  StoredProcedure [dbo].[mws_GetAllSubservicesDepartments]    Script Date: 09/09/2015 23:37:10 ******/
DROP PROCEDURE [dbo].[mws_GetAllSubservicesDepartments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ruslan Abdraev
-- Create date: 09/09/2015
-- Description:	Возвращает список всех услуг во всех организациях
-- =============================================
CREATE PROCEDURE [dbo].[mws_GetAllSubservicesDepartments]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
		  ,[service_id]
		  ,[organization_id]
		  ,[steps]
		  ,[details]
		  ,[price]
	  FROM [dbo].[ServiceOrganizationsLink]

END
GO
