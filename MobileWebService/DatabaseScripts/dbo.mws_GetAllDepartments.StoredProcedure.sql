/****** Object:  StoredProcedure [dbo].[mws_GetAllDepartments]    Script Date: 09/09/2015 23:37:10 ******/
DROP PROCEDURE [dbo].[mws_GetAllDepartments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ruslan Abdraev
-- Create date: 09/09/2015
-- Description:	Возвращает список всех организаций
-- =============================================
CREATE PROCEDURE [dbo].[mws_GetAllDepartments]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT [id]
		  ,[name]
		  ,[address]
		  ,[description]
		  ,[phone]
		  ,[city]
		  ,[email]
		  ,[website]
		  ,[parent_organization_id]
	  FROM [dbo].[Organizations]

END
GO
