using GET Method
----------------
http://<anyserver>:8090/JsonRpc2Service.rpc?jsonrpc={"method": "helloWorld", "params": ["Hello World"], "id": 1  }
    
using POST Method
-----------------
Headers:
POST http://localhost:49718/json.rpc HTTP/1.1
User-Agent: Fiddler
Content-Type: Application/Json-Rpc
Host: localhost:49718
Content-Length: 62

Body:
{"method": "helloWorld", "params": ["Hello World"], "id": 1  }

1. GetServiceGroups - возвращает все доступные категории
input 
{"method": "GetServiceGroups", "params": [], "id": 1  }

output 
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Service group 1","Description":"Service group description 1", "Icon":"http://91.213.233.179:8070/categories/justice.png","Tag":null,"DisplayFeedback":false}],"id":1}

2. GetAllServices - возвращает все услуги
input 
{"method": "GetAllServices", "params": [], "id": 1  }

output 
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Service 1","Description":"Service description 1","Service_Group_Id":1},{"Id":2,"Name":"Service 2","Description":"Service description 2","Service_Group_Id":2}],"id":1}

3. GetAllSubservices - возвращает список всех подуслуг(пломбирование, отбеливание зубов и т.д.)
input
{"method": "GetAllSubservices", "params": [], "id": 1  }

output
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Subservice 1","Description":"Subservice description 1","Service_Id":1, "Tag":null},{"Id":2,"Name":"Subservice 2","Description":"Subservice description 2","Service_Id":2, "Tag":null}],"id":1}

4. GetAllDepartments - возвращает список всех организаций
input
{"method": "GetAllDepartments", "params": [], "id": 1  }

output
{
	"jsonrpc":"2.0"
	,"result":[
				{
					"Id": 414
					,"Name": "Договор о безвозмездном оказании услуг"
					,"Address": ""
					,"Latitude": 0
					,"Longitude": 0
					,"Description": ""
					,"Phones": " "
					,"City": null
					,"Email": ""
					,"Website": ""
					,"Parent_Department_Id": null
					,"PhotoName": "HTTP://91.213.233.179:8070/ORGANIZATIONS/NOIMAGE.PNG"
					,"Tag": null
					,"DownloadLink": "HTTP://91.213.233.179:8070/DOCS/PROBONO_AGREEMENT.DOCX"
				}
				,{
					"Id": 691
					,"Name": "Дополнение к исковому заявлению"
					,"Address": ""
					,"Latitude": 0
					,"Longitude": 0
					,"Description": ""
					,"Phones": ""
					,"City": null
					,"Email": ""
					,"Website": ""
					,"Parent_Department_Id": null
					,"PhotoName": "HTTP://91.213.233.179:8070/ORGANIZATIONS/690.PNG"
					,"Tag": null
					,"DownloadLink": null
				}
			]
	,"id":1
}

5. GetAllSubservicesDepartments - список всех услуг во всех организациях
input
{"method": "GetAllSubservicesDepartments", "params": [], "id": 1  }

output 
{"jsonrpc":"2.0","result":[{"Id":1,"Service_Id":1,"Department_Id":1,"Steps":"","Details":"фывафыва","Price":1.0}],"id":1}

6. GetAllCities - список всех городов
input
{"method": "GetAllCities", "params": [], "id": 1  }

output
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Бишкек","District_Id":null}],"id":1}

7. GetAllDistricts - список всех районов
input
{"method": "GetAllDistricts", "params": [], "id": 1  }

output
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Alamedin","Region_Id":1}],"id":1}

8. GetAllRegions - список всех районов
input
{"method": "GetAllRegions", "params": [], "id": 1  }
output
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Chuy"}],"id":1}

9. GetVersionInfo - список всех районов
input
{"method": "GetVersionInfo", "params": [], "id": 1  }
output
{"jsonrpc":"2.0","result":2,"id":1}

10. GetFeedbackAnswers
input
{"method": "GetFeedbackAnswers", "params": [], "id": 1  }
output
{"jsonrpc":"2.0","result":[{"Id":1,"Name":"Отлично"},{"Id":4,"Name":"Плохо"},{"Id":3,"Name":"Средне"},{"Id":5,"Name":"Ужасно"},{"Id":2,"Name":"Хорошо"}],"id":1}

11. SetFeedback
input
{"method": "SetFeedback", "params": [1, 224, "Name", "Phone", "Comment text"], "id": 1  } 
где 1 - id feedback, 224 - id SubserviceOrganization, "Comment text" - текст коментария 
output
{"jsonrpc":"2.0","id":1}